# Branch

## Explosion Drawing
<img src="docs/branch_ex.png"  width="677" height="400">

### Plexiglas parts
-	Part 1: Base cover

### Connecting Parts 
-   Epoxy glue

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |
| ------    | ------                | ------    | ------            | ------    |
|   1       | ACAG0801-2450-T       |   1       | ACAG0801-2450-T   | https://www.mouser.de/ProductDetail/ABRACON/ACAG0801-2450-T?qs=fAHHVMwC%252BbhdHzoIvMh2Og%3D%3D |
|   2       | Capacitor 100nF       |   5       | 06033C104KAT4A    | https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|   3       | Capacitor 10µF        |   2       | GRM188R60J106ME84J| https://www.mouser.de/ProductDetail/Murata-Electronics/GRM188R60J106ME84J?qs=jHkklCh7amjgoxFXBHHBCA%3D%3D |
|   4       | Capacitor 1,5pF       |   1       | C0603C159B3HACTU  | https://www.mouser.de/ProductDetail/KEMET/C0603C159B3HACTU?qs=W0yvOO0ixfHOQrRW0H%252BsvA%3D%3D |
|   5       | Capacitor 1µF         |   28      | 02016D104KAT2A    | https://www.mouser.de/ProductDetail/AVX/02016D104KAT2A?qs=PN7sAUOUrntlfwxbZzo6OQ%3D%3D |
|   6       | IN-S124ARUW           |   28      | IN-S124ARUW       | https://www.mouser.de/ProductDetail/Inolux/IN-S124ARUW?qs=%2Fha2pyFaduhFrQeiBMucr%2FYDeGi3hh3b7zHutGmldXwobmQmLzwsWQ%3D%3D |
|   7       | TLW-106-06-G-D        |   2       | TLW-106-06-G-D    | https://www.mouser.de/ProductDetail/Samtec/TLW-106-06-G-D?qs=Oz4yqIfk2vLuQ68gvIKwvw%3D%3D |
|   8       | Connector PROG        |   1       | 20021511-00006T4LF| https://www.mouser.de/ProductDetail/Amphenol-FCI/20021511-00006T4LF?qs=%2Fha2pyFaduj%2Fipt9xra2X7w9g1wuSqbzBLkT1LceF%2FnU8fY47ySEttXVb41GLTyl |
|   9       | Inductor 4,3nH        |   1       | |  |
|   10      | 2N7002WT1G            |   28      | 2N7002WT1G        | https://www.mouser.de/ProductDetail/ON-Semiconductor/2N7002WT1G?qs=%2Fha2pyFadujPTHSYx5%2FnTbaF5MlOOaLYuqobFODOT4c%3D |
|   11      | Resistor 4,7k Ohm     |   39      | CRCW06034K70FKEA  | https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|   12      | Resistor 80 Ohm       |   28      | ERJ-3EKF80R6V     | https://www.mouser.de/ProductDetail/Panasonic/ERJ-3EKF80R6V?qs=%2Fha2pyFaduglTbPDDZSypdojfHHtTpVMCJsBXOPSqw%252B%252BQEm3Bf%2FUPA%3D%3D |
|   13      | Resistor 50m Ohm      |   2       | LVK12R050CER      | https://www.mouser.de/ProductDetail/Ohmite/LVK12R050CER?qs=%2Fha2pyFaduj7XFfIkG4bGop58u%2Fg9RC6%252Bnv9AX2esgFqkJqz4lozxw%3D%3D |
|   14      | MIC5524-3.3YMT        |   1       | MIC5524-3.3YMT-TZ | https://www.mouser.de/ProductDetail/Microchip-Technology-Micrel/MIC5524-33YMT-TZ?qs=U6T8BxXiZAWnjLNT1jmoVA%3D%3D |
|   15      | ESP32-PICO-D4         |   1       | ESP32-PICO-D4     | https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-PICO-D4?qs=MLItCLRbWsw7MJlbN3HfdA%3D%3D |
|   16      | SN74LVC1G332DRY       |   3       | SN74LVC1G332DRY   | https://www.mouser.de/ProductDetail/Texas-Instruments/SN74LVC1G332DRYR?qs=5kMjoDYFkghIDWohx3UHNg%3D%3D |
|   17      | AH1390                |   1       | AH1390-HK4-7      | https://www.mouser.de/ProductDetail/Diodes-Incorporated/AH1390-HK4-7?qs=%2Fha2pyFadujuDmjr0UITlofSUFEsHd6PcfzQKoMX6CU%3D |
|   18      | INA233                |   2       | INA233AIDGSR      | https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A== |
|   19      | BUK6D230-80E          |   1       | BUK6D230-80EX     | https://www.mouser.de/ProductDetail/Nexperia/BUK6D230-80EX?qs=vLWxofP3U2x2kIFoKccGWQ%3D%3D |
|   20      | 74LVC2G04GM           |   1       | 74LVC2G04GM,115   | https://www.mouser.de/ProductDetail/Nexperia/74LVC2G04GM115?qs=me8TqzrmIYVF7lqCgvoP9A%3D%3D |
|   21      | DMP2005UFG            |   4       | DMP2005UFG-13     | https://www.mouser.de/ProductDetail/Diodes-Incorporated/DMP2005UFG-13?qs=AQlKX63v8RvtpshLYRRk6A%3D%3D |


## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/distribution/branch/branch.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/grid/branch/branch.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/branch/branch.md) 