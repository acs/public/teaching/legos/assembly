# Node

## Explosion Drawing
<img src="docs/node_ex.png"  width="349" height="400">

### Plexiglas parts
-	Part 1: Base cover
- 	Part 2: Plug

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/distribution/node/node.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/grid/node/node.md)