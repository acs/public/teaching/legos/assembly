# Supply

## Explosion Drawing
<img src="docs/supply_ex.png"  width="361" height="400">

### Plexiglas parts
-	Part 1: Base cover

## Related Links
* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/distribution/supply/supply.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/grid/supply/supply.md)