# Hospital

## Explosion Drawing
<img src="docs/hospital_ex.png"  width="332" height="400">

### 3D parts
-	Part 1: Base
-	Part 2: Column
-	Part 3: Back
-	Part 4: Floor
-	Part 5: Roof + pressfit
-	Part 6: Battery cover

### Plexiglas
-	Plexiglas 25x28x3 mm [Rect]
-	Plexiglas 50x28x3 mm [Rect]

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws
-	Magnets

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |
| ------    | ------ 					| ------ 	| ------			| ------	|
|	1		| Screw M1.6				|	5 		| 					|			|
|	2    	| Plexiglas trn 25x28x3 mm 	|   4		|			       	| 			|
|	3		| Plexiglas trn 50x28x3 mm 	|   4		|       			|  			|
|	4		| Magnets 3x3x0.5mm		    | 	10   	| 					| https://www.neomagnete.de/de/quadermagnet-3x3x0-5mm-neodym-n48?c=6 |
|	5		| Si2301BDS					|	2		| SI2301BDS-T1-E3	| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2301BDS-T1-GE3?qs=tWM4QuTcS4VUQRxq4xvp7w%3D%3D |
|	6		| IN-S126ATR				|	8		| IN-S126ATR		| https://www.mouser.de/ProductDetail/Inolux/IN-S126ATR?qs=%2Fha2pyFadujcmMPeskJ8VRjXpiffnQ0ty1RGvy2OxlnlSpFGSOpZkw%3D%3D |
|	7		| AH1912_W					|	2		| AH1912-W-7		| https://www.mouser.de/ProductDetail/Diodes-Incorporated/AH1912-W-7?qs=Zz7%252BYVVL6bFXiouyTQgmPw%3D%3D |
|	8		| Resistor 31,6 Ohm			|	2		| CR0603-FX-31R6ELF	| https://www.mouser.de/ProductDetail/Bourns/CR0603-FX-31R6ELF?qs=%2Fha2pyFadugVmr2gairtN0HlcVQXOMF1BQZ5R2xAI%252BmmvsaVnpZ59Q%3D%3D |
|	9		| Resistor 10k Ohm			|	9		| CRCW060310K0JNEBC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|	10		| Resistor 3,3 Ohm			|	1		| CRCW06033R30FKEA	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06033R30FKEA?qs=aVQbgkeNPKB%2F6MYhEOKCtg%3D%3D |
|	11		| Resistor 5 Ohm			|	1		| ERJ-PA3J5R1V		| https://www.mouser.de/ProductDetail/Panasonic/ERJ-PA3J5R1V?qs=%2Fha2pyFadujfhmhVngkC6%2FRsbeEP6wTrJpwDYwLerY7IZ%2FOh2vpf |
|	12		| Resistor 100m Ohm			|	1		| KDV12DR100ET		| https://www.mouser.de/ProductDetail/Ohmite/KDV12DR100ET?qs=%2Fha2pyFadujYEP9Yjf896LY%252BB6oL%2FcSG2HG0xFKEjpbehyNOlB0Tog%3D%3D |
|	13		| Resistor 4,7k Ohm			|	5		| CRCW06034K70FKEA	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|	14		| Resistor 1k Ohm			|	1		| CRCW06031K00FKEAC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06031K00FKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLVmyhq699xnzTj0BYiFB3%2FaLM9ykHyj5ejAgw8293GI7 |
|	15		| Resistor 4,3 Ohm 1/2W		|	1		| ESR10EZPF4R30		| https://www.mouser.de/ProductDetail/ROHM-Semiconductor/ESR10EZPF4R30?qs=%2Fha2pyFadui7JatYYUNpFggwBPNB%252Bs69EZKnkUXQ03FU%2FdpiqSxO2w%3D%3D |
|	16		| Resistor 22 Ohm 1/4W		|	3		| KTR18EZPJ220		| https://www.mouser.de/ProductDetail/ROHM-Semiconductor/KTR18EZPJ220?qs=%2Fha2pyFadugzPinfS2vZITh%252BYEohsmUGOzLVLQxpTMZ0YhtTs0V0Kw%3D%3D |
|	17		| Capacitor 100nF			|	4		| 06033C104KAT4A	| https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|	18		| Battery Holder			|	1		| 1042 				| https://www.mouser.de/ProductDetail/Keystone-Electronics/1042?qs=%2F7TOpeL5Mz4qPdWi9tuLKw%3D%3D |
|	19		| Capacitor 10μF			|	1		| GRM188R60J106ME84J| https://www.mouser.de/ProductDetail/Murata-Electronics/GRM188R60J106ME84J?qs=jHkklCh7amjgoxFXBHHBCA%3D%3D |
|	20		| Si2302CDS					|	10		|					| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%252BPu8jn5UVnHNrjAmGCs%2Fuw%3D%3D |
|	21		| LED (OVLAW4CB7)			|	1		| 					| https://www.mouser.de/ProductDetail/Optek-TT-Electronics/OVLAW4CB7?qs=%2Fha2pyFadujzdKCSYOCF%2Fb%2F4YkU%2FwT0oK9SyE8dg0Io%3D |
|	22		| LED (IN-S126AT5UW)		|	18		| 					| https://www.mouser.de/ProductDetail/Inolux/IN-S126AT5UW?qs=%2Fha2pyFadujcmMPeskJ8Vctm2Py%2Ft%2Frfmi0kzFIvJQCC2eq40OLaYw%3D%3D |
|	23		| INA233					|	1		| 					| https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D |
|	24		| TLV803E					|	2		| 					| https://www.mouser.de/ProductDetail/Texas-Instruments/TLV803EA26DPWR?qs=W%2FMpXkg%252BdQ7k%252BxQuvIufAw%3D%3D |
|	25		| 18650 LiFoPo4				|	1		| 18650-S			| https://www.conrad.de/de/p/3-2-volt-solar-akku-lithium-18650-ifr-lifepo4-akku-mit-kopf-ungeschuetzt-1400-1500mah-abmessungen-ca-66-1x18mm-803842313.html |
|	26		| BAT60A					|	1		| 					| https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D |
|	27		| esp32						|	1		| 					| https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH2800PH3Q0?qs=sGAEpiMZZMu3sxpa5v1qruuGqP4jgT%2FvnmTo7KFPAbE%3D |
|	28		| PTS815					|	1		|					| https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=ahcBuItHZ3xKWmfV%2F2E6bA%3D%3D |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/hospital/hospital.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/hospital/hospital.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/hospital/hospital.md) 