# Stadium

## Explosion Drawing
<img src="docs/stadium_ex.png"  width="453" height="400">

### 3D parts
-	Part 1: Base
-	Part 2: Walls
-	Part 3: Roof
-	Part 4: Speaker cover

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |
|-----  | ------ 				| ------ 	| ------	    	| ------	|
|   1   | Capacitor 10µF		|	3		| GRM188R60J106ME84J| https://www.mouser.de/ProductDetail/Murata-Electronics/GRM188R60J106ME84J?qs=jHkklCh7amjgoxFXBHHBCA%3D%3D |
|   2   | Capacitor 100nF		|	3	    | 06033C104KAT4A	| https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|   3   | Capacitor 220pF		|  	2    	| 06033A221JAT2A	| https://www.mouser.de/ProductDetail/AVX/06033A221JAT2A?qs=icS7CTEoV1ML9Af1nc8Zaw%3D%3D |
|   4   | BAT60A				|   1   	| BAT60AE6327HTSA1	| https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D |
|   5   | IN-S124ARUR			|	12 	    | IN-S124ARUR		| https://www.mouser.de/ProductDetail/Inolux/IN-S124ARUR?qs=%2Fha2pyFaduhFrQeiBMucrygTY5g538LeJ5PMLteJG1R%252BJUtNZzefDQ%3D%3D |
|   6   | IN-S124ARG			|	12		| IN-S124ARG		| https://www.mouser.de/ProductDetail/Inolux/IN-S124ARG?qs=%2Fha2pyFaduhFrQeiBMucr1Ze%252BF19oDJZBc%2FG4z7Vmy9g7SgCL8emcA%3D%3D |
|   7   | IN-S126AT5UW			|	18		| IN-S126AT5UW		| https://www.mouser.de/ProductDetail/Inolux/IN-S126AT5UW?qs=%2Fha2pyFadujcmMPeskJ8Vctm2Py%2Ft%2Frfmi0kzFIvJQCC2eq40OLaYw%3D%3D |
|   8   | Ferrite 0,15H			|	2		| BK1608HW121-T		| https://www.mouser.de/ProductDetail/Taiyo-Yuden/BK1608HW121-T?qs=%2Fha2pyFadujRsLcit%252Be%252BSbcpUPnmEspN7BV7yJwyPpmyJ%2Fy4zTIwQA%3D%3D |
|   9	| MountingHole			|	6		| 97730356332R		| https://www.mouser.de/ProductDetail/Wurth-Elektronik/97730356332R?qs=%2Fha2pyFaduhh7j50G6IZ4FwQQt1JrBnGUNliCy6ozjs4NQ5%2FL59kQA%3D%3D |
|	10	| MAX98357AETE			|	1		| MAX98357AETE+T	| https://www.mouser.de/ProductDetail/Maxim-Integrated/MAX98357AETE%2bT?qs=AAveGqk956HhNpoJjF5x2g%3D%3D |
|	11	| Connector JST/SR		|	5		| 455-1788-1-ND		| https://www.digikey.de/products/de?keywords=455-1788-1-ND |
|	12	| Connector JST/SH		|	4		| 1832-1049-ND		| https://www.mouser.de/ProductDetail/TinyCircuits/ASR00015-50?qs=%2Fha2pyFadugq5V0W1QxUh5%2FX20cesp9yCOUfAy7iEohGx00p8p7vVA%3D%3D |
|	13	| Connector PROG		|	1		| 20021511-00006T4LF| https://www.mouser.de/ProductDetail/Amphenol-FCI/20021511-00006T4LF?qs=%2Fha2pyFaduj%2Fipt9xra2X7w9g1wuSqbzBLkT1LceF%2FnU8fY47ySEttXVb41GLTyl |
|	14	| Connector Pogo		|	10		| 0908-9-15-20-75-14-11-0| https://www.mouser.de/ProductDetail/Mill-Max/0908-9-15-20-75-14-11-0?qs=%2Fha2pyFaduhtbXauCNY5uTRkR8EFNEPBXMOO3bv7XLa7djLJN1kijCHR6hOAblv0 |
|	15	| Micro SD-Card Slot	|	1		| 693071020811		| https://www.mouser.de/ProductDetail/Wurth-Elektronik/693071020811?qs=%2Fha2pyFaduhEcrW2kuZj9535IQvUXnHGawIq1CU29Q1FvJZgyLQZLA%3D%3D |
|	16	| Si2302CDS				|	3		| SI2302CDS-T1-E3	| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%2Fha2pyFaduhn77Cd8sORMCPlOQ64g5p%252BNTSOwwQoaGlZHLEpIgs66Q%3D%3D |
|	17	| Resistor 10k Ohm		|	10		| CRCW060310K0JNEBC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|	18	| Resistor 100m Ohm		|	1		| KDV12DR100ET		| https://www.mouser.de/ProductDetail/Ohmite/KDV12DR100ET?qs=%2Fha2pyFadujYEP9Yjf896LY%252BB6oL%2FcSG2HG0xFKEjpbehyNOlB0Tog%3D%3D |
|	19	| Resistor 4,7k Ohm		|	4		| CRCW06034K70FKEA	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|	20	| Resistor 100k Ohm		|	1		| CRCW0603100KFKEAC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW0603100KFKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLcw2h53PIEbpqmPrufcnxqrQ5neTgziOAfer%252BU385wIB |
|	21	| Resistor 120 Ohm		|	12		| RC0603JR-7W120RL	| https://www.mouser.de/ProductDetail/Yageo/RC0603JR-7W120RL?qs=%2Fha2pyFaduiI%252B%2F%2Fp%2FYhTTYcnC%252BHD46zBj1j1sJh3MoaRQ8BEO%252B8qTw%3D%3D |
|	22	| Resistor 20 Ohm		|	30		| SDR03EZPJ200		| https://www.mouser.de/ProductDetail/ROHM-Semiconductor/SDR03EZPJ200?qs=%2Fha2pyFaduj8ju4lCAzuYqXVu6SzA0hARb0ZqeVpcHd67TMFnSadeA%3D%3D |
|	23	| PTS815				|	1		| PTS815 SJM 250 SMTR LFS| https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=%2Fha2pyFaduhI5UFm74GJ4OWa%2FUrmw2fJIH2fZNLZK%2Fapflh8XMB109Sdai2%252Bj7mo |
|	24	| TLV803E				|	1		| TLV803EA29DBZR	| https://www.mouser.de/ProductDetail/Texas-Instruments/TLV803EA29DBZR?qs=%2Fha2pyFaduhy7%252BEaq%2FQefMIt2a37GDy09phmfYeDkgETVdfWuSzdnQ%3D%3D |
|	25	| ESP-WROOM-32D			|	1		| ESP32-WROOM-32D(M113DH3200PH3Q0)| https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH3200PH3Q0?qs=%2Fha2pyFadugKGKyA%252BZerg2B0xz1dlPNH74rLf7O9OSvWVa4574SRV7OWtyNwqCoDyQsmrbbIWmE%3D |
|	26	| INA233				|	1		| INA233AIDGSR		| https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D |
|	27	| Speaker				|	1		| CDS-25148			| https://www.mouser.de/ProductDetail/CUI-Devices/CDS-25148?qs=8SF9RhewYjQKGy12vYPWxg%3D%3D |
|	28	| Srew M1.6				|	4		|				| [Screw](https://www.screwsandmore.de/en/product-range/screws-and-bolts/hexagon-socket-screws/countersunk-din-7991/din-7991-stainless-steel-a2/din-7991-a2-m1-6-en-negro/din-7991-a2-m1-6x4-sb/14651/50-pcs-din-7991-a2-m1-6x4-black |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/stadium/stadium.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/stadium/stadium.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/stadium/stadium.md) 