# Power Plant

## Explosion Drawing
<img src="docs/power_plant_ex.png"  width="291" height="400">

### 3D parts
-	Part 1: base
-	Part 2: walls
-	Part 3: roof
-	Part 4: chimeny
-	Part 5: turbine

### Plexiglas
-	Plexiglas 47x36x3 mm [Rect]
-	Plexiglas 50x36x3 mm [Rect]
-	Plexiglas 50x50x3 mm [Triang]

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws
-	Magnets

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |
|-----	| ------ 				| ------ 	| ------	    		| ------	|
|	1	| Plexiglas trn 47x36x3 mm | 1		|						|  |
|	2	| Plexiglas trn 50x36x3 mm | 1		|						|  |
|	3	| Plexiglas trn 50x50x3 mm| 1		|						|  |
|	4	| Epoxid Resin			|	1		|						|  |
|	5	| Capacitor 100nF		|	10		| 06033C104KAT4A		| https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|	6	| Capacitor 10µF		|	1		| GRM188R60J106ME84J	| https://www.mouser.de/ProductDetail/Murata-Electronics/GRM188R60J106ME84J?qs=jHkklCh7amjgoxFXBHHBCA%3D%3D |
|	7	| Capacitor 4,7µF		|	2		| CL10A475KQ8NNNC		| https://www.mouser.de/ProductDetail/Samsung-Electro-Mechanics/CL10A475KQ8NNNC?qs=%2Fha2pyFaduhvNw6MPmRrbQWZseRBqux1Ql9h2djGy274VVC%252BAk86Cg%3D%3D |
|	8	| Capacitor 100µF		|	2		| TLJA107M004R0500 		| https://www.mouser.de/ProductDetail/AVX/TLJA107M004R0500?qs=%2Fha2pyFadujTuboaZwHWz7DloL6LFca9nKUaIEFzt%252BCb5bPSnYBQmw%3D%3D |
|	9	| Capacitor 100pF		|	1		| 06035A101JAT4A		| https://www.mouser.de/ProductDetail/AVX/06035A101JAT4A?qs=I5rfTV0A4wFKHmoR%252B7UVKA%3D%3D |
|	10	| BAT60A				|	3		| BAT60AE6327HTSA1		| https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D |
|	11	| IN-S126ATR			|	6		| IN-S126ATR			| https://www.mouser.de/ProductDetail/Inolux/IN-S126ATR?qs=%2Fha2pyFadujcmMPeskJ8VRjXpiffnQ0ty1RGvy2OxlnlSpFGSOpZkw%3D%3D |
|	12	| IN-S126ATY			|	6		| IN-S126ATY			| https://www.mouser.de/ProductDetail/Inolux/IN-S126ATY?qs=u4fy%2FsgLU9MVJ3B3BIeTVw%3D%3D |
|	13	| IN-S126ATG			|	6		| IN-S126ATG			| https://www.mouser.de/ProductDetail/Inolux/IN-S126ATG?qs=%2Fha2pyFadujcmMPeskJ8VQtzLw0iNIxQMobziwJ0ZctbmLOhFh5Zhg%3D%3D |
|	14	| Mounting Hole			|	6		| 97730356332R			| https://www.mouser.de/ProductDetail/Wurth-Elektronik/97730356332R?qs=%2Fha2pyFaduhh7j50G6IZ4FwQQt1JrBnGUNliCy6ozjs4NQ5%2FL59kQA%3D%3D |
|	15	| 7106DG				|	2		| 7106DG				| https://www.mouser.de/ProductDetail/Aavid/7106DG?qs=IactFO9j9EBmsKLepV0tHg%3D%3D |
|	16	| Connector Prog		|	1		| 20021511-00006T4LF	| https://www.mouser.de/ProductDetail/Amphenol-FCI/20021511-00006T4LF?qs=%2Fha2pyFaduj%2Fipt9xra2X7w9g1wuSqbzBLkT1LceF%2FnU8fY47ySEttXVb41GLTyl |
|	17	| Connector Pogo		|	22		| 0908-9-15-20-75-14-11-0 | https://www.mouser.de/ProductDetail/Mill-Max/0908-9-15-20-75-14-11-0?qs=%2Fha2pyFaduhtbXauCNY5uTRkR8EFNEPBXMOO3bv7XLa7djLJN1kijCHR6hOAblv0 |
|	18	| Fan 35mm				|	1		| MF35060V1-1000U-A99	| https://www.mouser.de/ProductDetail/Sunon/MF35060V1-1000U-A99?qs=%2Fha2pyFadugeJPtK%2FTpNyLMZ1%252BaDNrCZcgRJ1%2Fyofrzo9YHv7H6bTVexxo5eCk7L |
|	19	| ASR00016				|	1		| ASR00016				| https://www.mouser.de/ProductDetail/TinyCircuits/ASR00016?qs=%2Fha2pyFaduhDRatjjMvjd0T5UksQvVCcrSAStndOGWc%3D |
|	20	| Si2302CDS				|	5		| SI2302CDS-T1-E3		| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%2Fha2pyFaduhn77Cd8sORMCPlOQ64g5p%252BNTSOwwQoaGlZHLEpIgs66Q%3D%3D |
|	21	| Si2301BDS				|	1		| SI2301BDS-T1-E3		| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2301BDS-T1-E3?qs=%2Fha2pyFaduj3XLid0RWuwc6M6sr4X6TDa51gm78P1G9joMb%252BNZQ9ww%3D%3D |
|	22	| Resistor 10k Ohm		|	6		| CRCW060310K0JNEBC		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|	23	| Resistor 4,7k Ohm		|	4		| CRCW06034K70FKEA		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|	24	| Resistor 1,25k Ohm	|	1		| CRCW06031K24FKEA		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06031K24FKEA?qs=oFi4%2FQsgZXZzdxcw%252Bz4oYQ%3D%3D |
|	25	| Resistor 30k Ohm		|	1		| SDR03EZPJ303			| https://www.mouser.de/ProductDetail/ROHM-Semiconductor/SDR03EZPJ303?qs=%2Fha2pyFaduj8ju4lCAzuYtJFifitWC0fkLcSh9vrAxE%2FGq%252BT1PF1OA%3D%3D |
|	26	| Resistor 5k Ohm		|	1		| CRCW06034K99FKEAC		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K99FKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLay5%252B%252BZAd7KiiqWxFNkUMsO3DMqerW3N7TemFUGU0EZ2 |
|	27	| Resistor 1,3k Ohm		|	1		| CRCW06031K30FKEA		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06031K30FKEA?qs=1ghF3ZI8kS9RsDMRsQroWw%3D%3D |
|	28	| Resistor 20m Ohm		|	1		| LVK12R020CER			| https://www.mouser.de/ProductDetail/Ohmite/LVK12R020CER?qs=%2Fha2pyFaduhu8uzPZ614WMkZU2uwXwzXNF1mO6XZaHqJmaxHt%2FJqJw%3D%3D |
|	29	| Resistor 1M Ohm		|	3		| ERJ-3GEYJ105V			| https://www.mouser.de/ProductDetail/Panasonic/ERJ-3GEYJ105V?qs=%2Fha2pyFadugUp37UjO2jw0TUNySZ2T2WaWQ6tzJ359KuvoDUnF31xA%3D%3D |
|	30	| Resistor 392k Ohm		|	3		| CR0603-FX-3923ELF		| https://www.mouser.de/ProductDetail/Bourns/CR0603-FX-3923ELF?qs=%2Fha2pyFadugVmr2gairtN%252BQeqABL29qZL35fxZoHN7GK61vB0KeVIQ%3D%3D |
|	31	| Resistor 196k Ohm		|	2		| CR0603-FX-1963ELF		| https://www.mouser.de/ProductDetail/Bourns/CR0603-FX-1963ELF?qs=%2Fha2pyFadugVmr2gairtN6P%2FAKkDK%252BfjKpYpGYGBnlunF8Uu3DE64A%3D%3D |
|	32	| Resistor 110 Ohm		|	6		| AC0603FR-07110RL		| https://www.mouser.de/ProductDetail/Yageo/AC0603FR-07110RL?qs=%2Fha2pyFadujj9sgtHXfPQhx6YUripHZDzLA3Bnj8qWXnu6h%2FeLD8bg%3D%3D |
|	33	| Resistor 220 Ohm		|	6		| RC0603FR-07220RL		| https://www.mouser.de/ProductDetail/Yageo/RC0603FR-07220RL?qs=%2Fha2pyFaduhXXNW8qwNUNuak4o3NxN%252B3MSuLltcGYDCxSRUSLHo5Dg%3D%3D |
|	34	| Resistor 140 Ohm		|	6		| AC0603FR-07140RL		| https://www.mouser.de/ProductDetail/Yageo/AC0603FR-07140RL?qs=%2Fha2pyFadujj9sgtHXfPQoi22YJL2PAZJh4980WGH09ZdWAW1uWKDQ%3D%3D |
|	35	| PTS8115				|	1		| PTS815 SJM 250 SMTR LFS | https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=%2Fha2pyFaduhI5UFm74GJ4OWa%2FUrmw2fJIH2fZNLZK%2Fapflh8XMB109Sdai2%252Bj7mo |
|	36	| ESP32-WROOM-32D		|	1		| ESP32-WROOM-32D(M113DH3200PH3Q0) | https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH3200PH3Q0?qs=%2Fha2pyFadugKGKyA%252BZerg2B0xz1dlPNH74rLf7O9OSvWVa4574SRV7OWtyNwqCoDyQsmrbbIWmE%3D |
|	37	| AP2114HA				|	1		| AP2114HA-3.3TRG1		| https://www.mouser.de/ProductDetail/Diodes-Incorporated/AP2114HA-33TRG1?qs=%2Fha2pyFaduhMNFj8Kq2MI%252BCc4uxHDDDopEFND498zqimfTkb7WQQMg%3D%3D |
|	38	| AD8400ARZ10			|	1		| AD8400ARZ10			| https://www.mouser.de/ProductDetail/Analog-Devices/AD8400ARZ10?qs=%2FtpEQrCGXCwM8Fz00jiA6Q%3D%3D |
|	39	| AD8400ARZ1			|	1		| AD8400ARZ1			| https://www.mouser.de/ProductDetail/Analog-Devices/AD8400ARZ1?qs=%2FtpEQrCGXCyKa5RoJdb0qg%3D%3D |
|	40	| LT3081R				|	2		| LT3081ER#PBF			| https://www.mouser.de/ProductDetail/Analog-Devices/LT3081ERPBF?qs=hVkxg5c3xu9EsVcFcpdoiQ%3D%3D |
|	41	| INA233				|	1		| INA233AIDGSR			| https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D |
|	42	| LTC6992CS6-1			|	2		| LTC6992CS6-1#TRMPBF	| https://www.mouser.de/ProductDetail/Analog-Devices/LTC6992CS6-1TRMPBF?qs=hVkxg5c3xu%252B%2Fbwdp9fCldw%3D%3D |
|	43	| TMUX1219DBVR			|	2		| TMUX1219DBVR			| https://www.mouser.de/ProductDetail/Texas-Instruments/TMUX1219DBVR?qs=%2Fha2pyFadujejbVX%2FFJbZFJwPh5QrcCoJzKf%252BjVhrnFfbJDGaZ62Fw%3D%3D |
|	44	| TLV4041R5				|	1		| TLV4041R5DBVR			| https://www.mouser.de/ProductDetail/Texas-Instruments/TLV4041R5DBVR?qs=%2Fha2pyFaduhb%252B2m165vMf3exSaisAM0B%2FK2N3fNC9qamB%252Bhuu74J%2Fw%3D%3D |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/power_plant/power_plant.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/power_plant/power_plant.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/power_plant/power_plant.md) 