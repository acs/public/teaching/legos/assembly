# Substation

## Explosion Drawing
<img src="docs/substation_ex.png"  width="293" height="400">

### 3D parts
-	Part 1: Base
-	Part 2: LED cover

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |
|-----  | ------ 				| ------ 	| ------					| ------	|
|   1   | M5 studding 		    |	1 		| 							|[Studding](https://www.bauhaus.info/gewindestangen/profi-depot-gewindestange-vz/p/10827362) |
|   1   | Screw M1.6			|	6 		|							|[Screw](https://www.screwsandmore.de/en/product-range/screws-and-bolts/hexagon-socket-screws/countersunk-din-7991/din-7991-stainless-steel-a2/din-7991-a2-m1-6-en-negro/din-7991-a2-m1-6x4-sb/14651/50-pcs-din-7991-a2-m1-6x4-black)|
|   3   | Resistor 220R 50mW 	|	6+ 		| RN50C2210FB14				|[Resistor](https://www.mouser.de/ProductDetail/Vishay-Dale/RN50C2210FB14?qs=6dqgS%252BKntIC6PQ%252B6nTYZyQ%3D%3D) |
|	4	| Resistor 10k Ohm		|	7		| CRCW060310K0JNEBC			| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|	5	| Resistor 220k Ohm		|	12		| RC0603FR-10220KL			| https://www.mouser.de/ProductDetail/Yageo/RC0603FR-10220KL?qs=%2Fha2pyFaduhXXNW8qwNUNniVXko%2F2rso1wWz%2Fe%2FJ72XFHyhjTASuhw%3D%3D |
|	6	| Resistor 140 Ohm		|	6		| AC0603FR-07140RL			| https://www.mouser.de/ProductDetail/Yageo/AC0603FR-07140RL?qs=%2Fha2pyFadujj9sgtHXfPQoi22YJL2PAZJh4980WGH09ZdWAW1uWKDQ%3D%3D |
|	7	| Resistor 130 Ohm		|	6		| RE0603FRE07130RL			| https://www.mouser.de/ProductDetail/Yageo/RE0603FRE07130RL?qs=%2Fha2pyFaduhqfBgHp1mwV9di35fY1B7GX5IwUz2v0GhGDxrQdQbbbQ%3D%3D |
|	8	| Resistor 4,7k Ohm		|	1		| CRCW06034K70FKEA			| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|	9	| Capacitor 220nF		|	1		| CC0603KRX7R7BB224			| https://www.mouser.de/ProductDetail/Yageo/CC0603KRX7R7BB224?qs=%2Fha2pyFaduh%252BUOWCkAJwPJWyDtgaXC3JBhUhkaRsvg38sa6%2FMFeSAw%3D%3D |
|	10	| Capacitor 4,7µF		|	2		| CL10A475KQ8NNNC			| https://www.mouser.de/ProductDetail/Samsung-Electro-Mechanics/CL10A475KQ8NNNC?qs=%2Fha2pyFaduhvNw6MPmRrbQWZseRBqux1Ql9h2djGy274VVC%252BAk86Cg%3D%3D |
|	11	| Capacitor 100nF		|	1		| 06033C104KAT4A			| https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|	12	| Capacitor 10µF		|	1		| GRM188R60J106ME84J		| https://www.mouser.de/ProductDetail/Murata-Electronics/GRM188R60J106ME84J?qs=jHkklCh7amjgoxFXBHHBCA%3D%3D |
|	13	| WP513IDT				|	6		| WP513IDT					| https://www.mouser.de/ProductDetail/Kingbright/WP513IDT?qs=%2Fha2pyFaduhXiyZASavXQtVrXf46fmB9zFPYAcjDrxQ= |
|	14	| WP513GDT				|	6		| WP513GDT					| https://www.mouser.de/ProductDetail/Kingbright/WP513GDT?qs=%2Fha2pyFadugOzaq%252B26tbQgpj5fK4IkRISN2MmfHGiM8%3D |
|	15	| BAT60A				|	1		| BAT60AE6327HTSA1			| https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D |
|	16	| MountingHole			|	6		| 97730356332R				| https://www.mouser.de/ProductDetail/Wurth-Elektronik/97730356332R?qs=%2Fha2pyFaduhh7j50G6IZ4FwQQt1JrBnGUNliCy6ozjs4NQ5%2FL59kQA%3D%3D |
|	17	| Prog Connector		|	1		| 20021511-00006T4LF		| https://www.mouser.de/ProductDetail/Amphenol-FCI/20021511-00006T4LF?qs=%2Fha2pyFaduj%2Fipt9xra2X7w9g1wuSqbzBLkT1LceF%2FnU8fY47ySEttXVb41GLTyl |
|	18	| Pogo Connector		|	10		| 0908-9-15-20-75-14-11-0	| https://www.mouser.de/ProductDetail/Mill-Max/0908-9-15-20-75-14-11-0?qs=%2Fha2pyFaduhtbXauCNY5uTRkR8EFNEPBXMOO3bv7XLa7djLJN1kijCHR6hOAblv0 |
|	19	| Si2302CDS				|	6		| SI2302CDS-T1-E3			| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%2Fha2pyFaduhn77Cd8sORMCPlOQ64g5p%252BNTSOwwQoaGlZHLEpIgs66Q%3D%3D |
|	20	| Si2301BDS				|	12		| SI2301BDS-T1-E3			| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2301BDS-T1-E3?qs=%2Fha2pyFaduj3XLid0RWuwc6M6sr4X6TDa51gm78P1G9joMb%252BNZQ9ww%3D%3D |
|	21	| PTS815				|	1		| PTS815 SJM 250 SMTR LFS	| https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=%2Fha2pyFaduhI5UFm74GJ4OWa%2FUrmw2fJIH2fZNLZK%2Fapflh8XMB109Sdai2%252Bj7mo |
|	22	| AP2114HA				|	1		| AP2114HA-3.3TRG1			| https://www.mouser.de/ProductDetail/Diodes-Incorporated/AP2114HA-33TRG1?qs=%2Fha2pyFaduhMNFj8Kq2MI%252BCc4uxHDDDopEFND498zqimfTkb7WQQMg%3D%3D |
|	23	| ESP32-WROOM-32D		|	1		| ESP32-WROOM-32D(M113DH3200PH3Q0) | https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH3200PH3Q0?qs=%2Fha2pyFadugKGKyA%252BZerg2B0xz1dlPNH74rLf7O9OSvWVa4574SRV7OWtyNwqCoDyQsmrbbIWmE%3D |
|	24	| TLV803E				|	1		| TLV803EA29DBZR			| https://www.mouser.de/ProductDetail/Texas-Instruments/TLV803EA29DBZR?qs=%2Fha2pyFaduhy7%252BEaq%2FQefMIt2a37GDy09phmfYeDkgETVdfWuSzdnQ%3D%3D |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/substation/substation.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/substation/substation.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/substation/substation.md) 