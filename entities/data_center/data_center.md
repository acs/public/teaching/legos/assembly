# Data Center

## Explosion Drawing
<img src="docs/data_center_ex.png"  width="364" height="400">

### 3D parts
-	Part 1: Base
- 	Part 2: Battery cover
-	Part 3: Roof

### Plexiglas
-	Plexiglas 60x70x3 mm [Rect]
-	Plexiglas 60x100x3 mm [Rect]
-	Plexiglas 70x100x3 mm [Triang]

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws
-	Magnets

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |
| ------    | ------ 				| ------ 	| ------				| ------	| 
|	1		| Screw M1.6			|	4		| 						| 			|
|	2		| 18650 LiFePO4			|	1		| 18650-S				| https://www.conrad.de/de/p/3-2-volt-solar-akku-lithium-18650-ifr-lifepo4-akku-mit-kopf-ungeschuetzt-1400-1500mah-abmessungen-ca-66-1x18mm-803842313.html |
|	3		| Plexiglas trn 60x70x3 mm| 2 		|						|			|
|	4		| Plexiglas trn 60x100x3 mm| 2 		|						|			|
|	5		| Plexiglas trn 70x100x3 mm| 1      |						|			|
|	6		| Micro SD Card slot	|	1		| 693071020811			| https://www.mouser.de/ProductDetail/Wurth-Elektronik/693071020811?qs=OlC7AqGiEDlbvtyrJEsiNg%3D%3D |
|	7		| IN-S126AT5UW			|	18		| IN-S126AT5UW			| https://www.mouser.de/ProductDetail/Inolux/IN-S126AT5UW?qs=%2Fha2pyFadujcmMPeskJ8Vctm2Py%2Ft%2Frfmi0kzFIvJQCC2eq40OLaYw%3D%3D |
|	8		| IN-S126ATB			|	6		| IN-S126ATB			| https://www.mouser.de/ProductDetail/Inolux/IN-S126ATB?qs=%2Fha2pyFadujcmMPeskJ8VasOrrwMh3b8NprSh8TiU4cGDhSVmNv5sA%3D%3D |
|	10		| Si2302CDS				|	20		| SI2302CDS-T1-E3		| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%252BPu8jn5UVnHNrjAmGCs%2Fuw%3D%3D |
|	11		| Resistor 10k Ohm		|	17		| CRCW060310K0JNEBC		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|	12		| Resistor 20 Ohm		|	12		| SDR03EZPJ200			| https://www.mouser.de/ProductDetail/ROHM-Semiconductor/SDR03EZPJ200?qs=%2Fha2pyFaduj8ju4lCAzuYqXVu6SzA0hARb0ZqeVpcHd67TMFnSadeA%3D%3D |
|	13		| INA233				|	1		| INA233AIDGSR			| https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D |
|	14		| Resistor 4,7k Ohm		|	5		| CRCW06034K70FKEA		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|	15		| Resistor 100m Ohm		|	1		| KDV12DR100ET			| https://www.mouser.de/ProductDetail/Ohmite/KDV12DR100ET?qs=%2Fha2pyFadujYEP9Yjf896LY%252BB6oL%2FcSG2HG0xFKEjpbehyNOlB0Tog%3D%3D |
|	16		| Capacitor 100nF		|	2		| 06033C104KAT4A		| https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|	17		| Resistor 22 Ohm 1/4W	|	3		| KTR18EZPJ220			| https://www.mouser.de/ProductDetail/ROHM-Semiconductor/KTR18EZPJ220?qs=%2Fha2pyFadugzPinfS2vZITh%252BYEohsmUGOzLVLQxpTMZ0YhtTs0V0Kw%3D%3D |
|	18		| Resistor 1k Ohm		|	1		| CRCW06031K00FKEAC		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06031K00FKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLVmyhq699xnzTj0BYiFB3%2FaLM9ykHyj5ejAgw8293GI7 |
|	19		| Resistor 4,3 Ohm 1/2W	|	1		| ESR10EZPF4R30			| https://www.mouser.de/ProductDetail/ROHM-Semiconductor/ESR10EZPF4R30?qs=%2Fha2pyFadui7JatYYUNpFggwBPNB%252Bs69EZKnkUXQ03FU%2FdpiqSxO2w%3D%3D |
|	20		| TLV803E				|	2		| TLV803EA29DBZR		| https://www.mouser.de/ProductDetail/Texas-Instruments/TLV803EA26DPWR?qs=W%2FMpXkg%252BdQ7k%252BxQuvIufAw%3D%3D |
|	21		| Battery Holder		|	1		| 1042					| https://www.mouser.de/ProductDetail/Keystone-Electronics/1042?qs=%2F7TOpeL5Mz4qPdWi9tuLKw%3D%3D |
|	22		| Capacitor 10μF 		|	2		| GRM188R60J106ME84J	| https://www.mouser.de/ProductDetail/Murata-Electronics/GRM188R60J106ME84J?qs=jHkklCh7amjgoxFXBHHBCA%3D%3D |
|	23		| PTS815				|	1		| 						| https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=ahcBuItHZ3xKWmfV%2F2E6bA%3D%3D |
|	24		| BAT60A				|	1		| BAT60AE6327HTSA1		| https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D |
|	25		| MountingHole			|	6		| 97730356332R			| https://www.mouser.de/ProductDetail/Wurth-Elektronik/97730356332R?qs=%2Fha2pyFaduhh7j50G6IZ4FwQQt1JrBnGUNliCy6ozjs4NQ5%2FL59kQA%3D%3D |
|	26		| Connector PROG		|	1		| 20021511-00006T4LF	| https://www.mouser.de/ProductDetail/Amphenol-FCI/20021511-00006T4LF?qs=%2Fha2pyFaduj%2Fipt9xra2X7w9g1wuSqbzBLkT1LceF%2FnU8fY47ySEttXVb41GLTyl |
|	27		| Connector Pogo		|	10		| 0908-9-15-20-75-14-11-0 | https://www.mouser.de/ProductDetail/Mill-Max/0908-9-15-20-75-14-11-0?qs=%2Fha2pyFaduhtbXauCNY5uTRkR8EFNEPBXMOO3bv7XLa7djLJN1kijCHR6hOAblv0 |
|	28		| ESP32					|	1		| ESP32-WROOM-32D(M113DH3200PH3Q0) | https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH3200PH3Q0?qs=%2Fha2pyFadugKGKyA%252BZerg2B0xz1dlPNH74rLf7O9OSvWVa4574SRV7OWtyNwqCoDyQsmrbbIWmE%3D |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/data_center/data_center.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/data_center/data_center.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/data_center/data_center.md) 