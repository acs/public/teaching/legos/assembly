# Wind Farm

## Explosion Drawing
<img src="docs/wind_farm.png"  width="197" height="400">

### 3D parts
-	Part 1: Base
-	Part 2: Socket

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |
|-----  | ------ 				| ------ 	| ------	    |
|   1   |   esp32              	|   1       |   https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH2800PH3Q0?qs=sGAEpiMZZMu3sxpa5v1qruuGqP4jgT%2FvnmTo7KFPAbE%3D			    |
|   2   |   Wind turbind        |   1   	|	https://www.hommel-gbr.de/spiel-und-freizeit/Kids-Globe/Hof-Zubehoer/Windmuehle-Windrad-weiss-mit-Batterie-fuer-Siku-1-32--Bauernhof-Kids-Globe-571897.html	        |
|   3   | 	AP2114HA	        |  	1    	|	https://www.mouser.de/ProductDetail/Diodes-Incorporated/AP2114HA-33TRG1?qs=M%2FOdCRO8QQ3KWhA9Gt6VoQ%3D%3D		    |
|   4   | 	LED	(IN-S124ARUW)   |  	5    	|  https://www.mouser.de/ProductDetail/Inolux/IN-S124ARUW?qs=%2Fha2pyFaduhFrQeiBMucr%2FYDeGi3hh3b7zHutGmldXwobmQmLzwsWQ%3D%3D |
|   5   | 	2N7002NXA          	| 	5    	|  https://www.mouser.de/ProductDetail/Nexperia/2N7002NXAKR?qs=%2Fha2pyFadugrT3yLZ3Uc%2FirSTYuUHokfVNR3jI8cbZSevCo7zrK0%252Bg%3D%3D             |
|	6	|	Si2302CDS			|	1		|	https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%252BPu8jn5UVnHNrjAmGCs%2Fuw%3D%3D			|
|	7	|	BAT60A				|	1		|	https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D			|
|	8	|	Resistor 80 Ohm 	|	5		| https://www.mouser.de/ProductDetail/Panasonic/ERJ-3EKF80R6V?qs=%2Fha2pyFaduglTbPDDZSypdojfHHtTpVMCJsBXOPSqw%252B%252BQEm3Bf%2FUPA%3D%3D |
|	9	|	Resistor 10k Ohm	|	7		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|	10	|	Resistor 100 Ohm	|	1		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW0603100RFKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLXlkDUXKZxW6R4mojuBr2YWpEWpG3X8Xg%252Bp2it%2F3qp%252BA |
|	11	|	INA233				|	1		| https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D |
|	12	|	Resistor 4,7k		|	3		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|	13	|	Capacitor 100nF		|	3		| https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|	14	|	Resistor 50m Ohm	|	1		| https://www.mouser.de/ProductDetail/Ohmite/LVK12R050CER?qs=%2Fha2pyFaduj7XFfIkG4bGop58u%2Fg9RC6%252Bnv9AX2esgFqkJqz4lozxw%3D%3D |
|	15	|	LT3081R				|	1		| https://www.mouser.de/ProductDetail/Analog-Devices/LT3081ERPBF?qs=hVkxg5c3xu9EsVcFcpdoiQ%3D%3D |
|	16	|	AD8400ARZ10			|	1		| https://www.mouser.de/ProductDetail/Analog-Devices/AD8400ARZ10?qs=%2FtpEQrCGXCwM8Fz00jiA6Q%3D%3D |
|	17	|	Capacitor 100μF		|	2		| https://www.mouser.de/ProductDetail/KEMET/T491B107M006ATAUTO?qs=%2Fha2pyFaduiCg5z0qyjcWTJoKkRvcsbmKhkB74XJlwMxj12twBzPnSlbHHQolkIA |
|	18	|	Capacitor 30pF		|	2		| https://www.mouser.de/ProductDetail/AVX/06035A300JAT2A?qs=yuhjMS%2FIYxYp690hzqRzIg%3D%3D |
|	19	|	Capacitor 10μF		|	1		| https://www.mouser.de/ProductDetail/AVX/06035C103JAT2A?qs=aquwThhUVI9dzXPTvGj7jQ%3D%3D |
|	20	|	Capacitor 1μF		|	1		| https://www.mouser.de/ProductDetail/AVX/02016D104KAT2A?qs=PN7sAUOUrntlfwxbZzo6OQ%3D%3D |
|	21	|	Capacitor 4,7μF		|	2		| https://www.mouser.de/ProductDetail/Samsung-Electro-Mechanics/CL10A475KQ8NNNC?qs=%2Fha2pyFaduhvNw6MPmRrbQWZseRBqux1Ql9h2djGy274VVC%252BAk86Cg%3D%3D |
|	22	|	AP2114HA			|	1		| https://www.mouser.de/ProductDetail/Diodes-Incorporated/AP2114HA-33TRG1?qs=M%2FOdCRO8QQ3KWhA9Gt6VoQ%3D%3D |
|	23	|	PTS815				|	1		| https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=ahcBuItHZ3xKWmfV%2F2E6bA%3D%3D |
|	24	| Srew M1.6				|	5		| [Screw](https://www.screwsandmore.de/en/product-range/screws-and-bolts/hexagon-socket-screws/countersunk-din-7991/din-7991-stainless-steel-a2/din-7991-a2-m1-6-en-negro/din-7991-a2-m1-6x4-sb/14651/50-pcs-din-7991-a2-m1-6x4-black |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/wind_farm/wind_farm.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/wind_farm/wind_farm.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/wind_farm/wind_farm.md) 