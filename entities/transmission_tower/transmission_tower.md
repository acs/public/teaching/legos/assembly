#  Transmission Tower

## Explosion Drawing
<img src="docs/transmission_tower_ex.png"  width="263" height="400">

### 3D parts
-	Part 1: Base

### Connecting Parts 
-   Epoxy glue

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/transmission_tower/transmission_tower.md)