# Supermarket

## Explosion Drawing
<img src="docs/supermarket_ex.png"  width="366" height="400">

### 3D parts
-	Part 1: Base
-	Part 2: Freezers
-	Part 3: Roof
-	Part 4: Boiler

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws
-	Magnets

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |
|-----  | ------ 				| ------ 	| ------	|
|   1   | Plexiglas trn 96x50x3 mm| 2       |  |
|   2   | Plexiglas trn 40x50x3 mm| 2       |  |
|   3   | Capacitor 10µF        |	2    	| https://www.mouser.de/ProductDetail/AVX/0603ZD106KAT2A?qs=c1aAEamvUYHQhwHZOF6y0Q%3D%3D |
|   4   | Capacitor 100nF       |	5    	| https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|   5   | Capacitor 100µF      	|	2    	| https://www.mouser.de/ProductDetail/KEMET/T491B107M006ATAUTO?qs=%2Fha2pyFaduiCg5z0qyjcWTJoKkRvcsbmKhkB74XJlwMxj12twBzPnSlbHHQolkIA |
|	6	| Capacitor 30pF		|	1		| https://www.mouser.de/ProductDetail/AVX/06035A300JAT2A?qs=yuhjMS%2FIYxYp690hzqRzIg%3D%3D |
|	7	| Capacitor 1µF			|	2		| https://www.mouser.de/ProductDetail/AVX/02016D104KAT2A?qs=PN7sAUOUrntlfwxbZzo6OQ%3D%3D |
|	8	| Capacitor 1nF			|	1		| https://www.mouser.de/ProductDetail/AVX/06035C102KAT2A?qs=ariCldvIWN9RilgOv9HrZw%3D%3D |
|	9	| Capacitor 1,5mF		|	1		| https://www.mouser.de/ProductDetail/AVX/TPSE158K004R0075?qs=%2Fha2pyFaduh7HkaozZJ2BY9Kb%252BYXZ%2FuJIexq1E%252B1PXnn%2FlPDoREhKg%3D%3D |
|	10	| BAT60A				|	1		| https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D |
|	11	| IN-S126ATA			|	12		| https://www.mouser.de/ProductDetail/Inolux/IN-S126ATA?qs=%2Fha2pyFadujcmMPeskJ8VcG2n8UD6jTaBI3kstGv9bT7XccyLu%252B0xg%3D%3D |
|	12	| IN-S126ATB			|	10		| https://www.mouser.de/ProductDetail/Inolux/IN-S126ATB?qs=%2Fha2pyFadujcmMPeskJ8VasOrrwMh3b8NprSh8TiU4cGDhSVmNv5sA%3D%3D |
|	13	| IN-S126AT5UW			|	8		| https://www.mouser.de/ProductDetail/Inolux/IN-S126AT5UW?qs=%2Fha2pyFadujcmMPeskJ8Vctm2Py%2Ft%2Frfmi0kzFIvJQCC2eq40OLaYw%3D%3D |
|	14	| IN-S126ATR			|	2		| https://www.mouser.de/ProductDetail/Inolux/IN-S126ATR?qs=%2Fha2pyFadujcmMPeskJ8VRjXpiffnQ0ty1RGvy2OxlnlSpFGSOpZkw%3D%3D |
|	15	| ACSC56				|	3		| https://www.mouser.de/ProductDetail/Kingbright/ACSC56-41SRWA-F01?qs=%2Fha2pyFaduiRb1ou7ApVWdNKDmgVhPQNCXpSA0cxub%2FrV3dQMZJcGg%3D%3D |
|	16	| Si2302CDS				|	14		| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%2Fha2pyFaduhn77Cd8sORMCPlOQ64g5p%252BNTSOwwQoaGlZHLEpIgs66Q%3D%3D |
|	17	| SI2301BDS				|	4		| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2301BDS-T1-E3?qs=%2Fha2pyFaduj3XLid0RWuwc6M6sr4X6TDa51gm78P1G9joMb%252BNZQ9ww%3D%3D |
|	18	| Programing connector	|	1		|  |
|	19	| Pogoconnector			|	10		| https://www.mouser.de/ProductDetail/Mill-Max/0908-9-15-20-75-14-11-0?qs=%2Fha2pyFaduhtbXauCNY5uTRkR8EFNEPBXMOO3bv7XLa7djLJN1kijCHR6hOAblv0 |
|	20	| Mounting Hole			|	6		| https://www.mouser.de/ProductDetail/Wurth-Elektronik/97730356332R?qs=%2Fha2pyFaduhh7j50G6IZ4FwQQt1JrBnGUNliCy6ozjs4NQ5%2FL59kQA%3D%3D |
|	21	| Resistor 10k Ohm		|	12		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|	22	| Resistor 4,7k Ohm		|	7		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|	23	| Resistor 100m Ohm		|	2		| https://www.mouser.de/ProductDetail/Ohmite/KDV12DR100ET?qs=%2Fha2pyFadujYEP9Yjf896LY%252BB6oL%2FcSG2HG0xFKEjpbehyNOlB0Tog%3D%3D |
|	24	| Resistor 2,49 Ohm 400mW |	1		| https://www.mouser.de/ProductDetail/Vishay-Dale/RCS08052R49FKEA?qs=%2Fha2pyFaduisEDCP2eoct%252B0CaZBEsZCVa4JEC3AMYDTYHrRlumr04g%3D%3D |
|	25	| Resistor 120 Ohm		|	14		| https://www.mouser.de/ProductDetail/Yageo/RC0603JR-7W120RL?qs=%2Fha2pyFaduiI%252B%2F%2Fp%2FYhTTYcnC%252BHD46zBj1j1sJh3MoaRQ8BEO%252B8qTw%3D%3D |
|	26	| Resistor 180 Ohm		|	15		| https://www.mouser.de/ProductDetail/Panasonic/ERJ-3EKF1800V?qs=%2Fha2pyFaduglTbPDDZSypfTAdn6P%252BL1Urg0aUURqOm%252Bm49twFs0qdg%3D%3D |
|	27	| Resistor 2,1k Ohm		|	1		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06032K10FKEA?qs=cjTnrCMxNphZSCVQf3SAdw%3D%3D |
|	28	| Resistor 2,2k Ohm		|	1		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06032K20JNEAC?qs=%2Fha2pyFaduhQpfMWkxpuLSRNHr4l9fvDPxOB2gdt%252BkImgOM55T5G13zyi0EyMX7u |
|	29	| Resistor 50 Ohm		|	1		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060350R0FKEA?qs=%2Fha2pyFaduhQpfMWkxpuLQ5dIUVvKHg5SPan2l7USSu4rWgfTifudg%3D%3D |
|	30	| Resistor 196k Ohm		|	3		| https://www.mouser.de/ProductDetail/Bourns/CR0603-FX-1963ELF?qs=%2Fha2pyFadugVmr2gairtN6P%2FAKkDK%252BfjKpYpGYGBnlunF8Uu3DE64A%3D%3D |
|	31	| Resistor 200 Ohm		|	1		| https://www.mouser.de/ProductDetail/Yageo/RC0603FR-07200RL?qs=NiiRlmYYPUUE9K2wJmnrsQ%3D%3D |
|	32	| Resistor 20 Ohm		|	2		| https://www.mouser.de/ProductDetail/ROHM-Semiconductor/SDR03EZPJ200?qs=%2Fha2pyFaduj8ju4lCAzuYqXVu6SzA0hARb0ZqeVpcHd67TMFnSadeA%3D%3D |
|	33	| Resistor 330 Ohm		|	22		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW0603330RJNEAC?qs=%2Fha2pyFaduhQpfMWkxpuLWM32NMZehgZudmYL9BiHe12vCOHqB4%252BuBi1BTg6IUYe |
|	34	| Resistor 100k Ohm		|	2		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW0603100KFKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLcw2h53PIEbpqmPrufcnxqrQ5neTgziOAfer%252BU385wIB |
|	35	| Resistor NTC 680 Ohm	|	1		| https://www.mouser.de/ProductDetail/AVX/NC20MC0681KBA?qs=%2Fha2pyFaduhNRFNVCQePU3CBLybJTSBCYlSrkf%252BuDSS2VXNklpWXEw%3D%3D |
|	36	| PTS815				|	1		| https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=%2Fha2pyFaduhI5UFm74GJ4OWa%2FUrmw2fJIH2fZNLZK%2Fapflh8XMB109Sdai2%252Bj7mo |
|	37	| TLV803E				|	1		| https://www.mouser.de/ProductDetail/Texas-Instruments/TLV803EA29DBZR?qs=%2Fha2pyFaduhy7%252BEaq%2FQefMIt2a37GDy09phmfYeDkgETVdfWuSzdnQ%3D%3D |
|	38	| ESP32-WROM-32D		|	1		| https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH3200PH3Q0?qs=sGAEpiMZZMu3sxpa5v1qruuGqP4jgT%2FvoUDAcGeBvvY= |
|	39	| INA233				|	2		| https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D |
|	40	| AD8400ARZ1			|	1		| https://www.mouser.de/ProductDetail/Analog-Devices/AD8400ARZ1?qs=%2FtpEQrCGXCyKa5RoJdb0qg%3D%3D |
|	41	| LT3081R				|	1		| https://www.mouser.de/ProductDetail/Analog-Devices/LT3081ERPBF?qs=hVkxg5c3xu9EsVcFcpdoiQ%3D%3D |
|	42	| AP7313-10SAG-7		|	1		| https://www.mouser.de/ProductDetail/Diodes-Incorporated/AP7313-10SAG-7?qs=%2Fha2pyFadujy%2Fgnw%252B3xItMCP2PPkd2QqKwJawW5mbujts%252BUgrsB3SA%3D%3D |
|	43	| LTC6992C6-1			|	1		| https://www.mouser.de/ProductDetail/Analog-Devices/LTC6992CS6-1TRPBF?qs=hVkxg5c3xu%252BfQsFroZf77A%3D%3D |
|	44	| CD74HC4511PW			|	1		| https://www.mouser.de/ProductDetail/Texas-Instruments/CD74HC4511PWRE4?qs=xFfolx0DHx1qW9PAIFxsTQ%3D%3D |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/supermarket/supermarket.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/supermarket/supermarket.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/supermarket/supermarket.md) 