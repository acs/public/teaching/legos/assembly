# Solar Farm

## Explosion Drawing
<img src="docs/solar_farm_ex.png"  width="512" height="400">

### 3D parts
-	Part 1: Base
- 	Part 2: Servo cover
-	Part 3: Solar array

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws

### Components
|Part   | component 			| amount 	| manufacturer code	| link		    |
|-----  | ------ 				| ------ 	| ------	    	| ------		|
|   1   | Capacitor 1µF			|	3		| 02016D104KAT2A	| https://www.mouser.de/ProductDetail/AVX/02016D104KAT2A?qs=PN7sAUOUrntlfwxbZzo6OQ%3D%3D |
|   2   | Capacitor 100nF		|	7		| 06033C104KAT4A    | https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|   3   | Capacitor 10µF		|	1		| GRM188R60J106ME84J| https://www.mouser.de/ProductDetail/Murata-Electronics/GRM188R60J106ME84J?qs=jHkklCh7amjgoxFXBHHBCA%3D%3D |
|   4   | Capacitor 4,7µF		|	2		| CL10A475KQ8NNNC	| https://www.mouser.de/ProductDetail/Samsung-Electro-Mechanics/CL10A475KQ8NNNC?qs=%2Fha2pyFaduhvNw6MPmRrbQWZseRBqux1Ql9h2djGy274VVC%252BAk86Cg%3D%3D |
|   5   | Capacitor 100µF		|	2		| TLJA107M004R0500 	| https://www.mouser.de/ProductDetail/AVX/TLJA107M004R0500?qs=%2Fha2pyFadujTuboaZwHWz7DloL6LFca9nKUaIEFzt%252BCb5bPSnYBQmw%3D%3D |
|	6	| Capacitor 30pF		|	1		| 06035A300JAT2A	| https://www.mouser.de/ProductDetail/AVX/06035A300JAT2A?qs=yuhjMS%2FIYxYp690hzqRzIg%3D%3D |
|	7	| Capacitor 1nF			|	1		| 06035C102KAT2A	| https://www.mouser.de/ProductDetail/AVX/06035C102KAT2A?qs=ariCldvIWN9RilgOv9HrZw%3D%3D |
|	8	| BAT60A				|	1		| BAT60AE6327HTSA1	| https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D |
|	9	| IN-S126AT5UW			|	5		| IN-S126AT5UW		| https://www.mouser.de/ProductDetail/Inolux/IN-S126AT5UW?qs=%2Fha2pyFadujcmMPeskJ8Vctm2Py%2Ft%2Frfmi0kzFIvJQCC2eq40OLaYw%3D%3D |
|	10	| Mounting Hole			|	6		| 97730356332R		| https://www.mouser.de/ProductDetail/Wurth-Elektronik/97730356332R?qs=%2Fha2pyFaduhh7j50G6IZ4FwQQt1JrBnGUNliCy6ozjs4NQ5%2FL59kQA%3D%3D |
|	11	| Connector Prog		|	1		| 20021511-00006T4LF| https://www.mouser.de/ProductDetail/Amphenol-FCI/20021511-00006T4LF?qs=%2Fha2pyFaduj%2Fipt9xra2X7w9g1wuSqbzBLkT1LceF%2FnU8fY47ySEttXVb41GLTyl |
|	12	| Connector Pogo		|	10		| 0908-9-15-20-75-14-11-0| https://www.mouser.de/ProductDetail/Mill-Max/0908-9-15-20-75-14-11-0?qs=%2Fha2pyFaduhtbXauCNY5uTRkR8EFNEPBXMOO3bv7XLa7djLJN1kijCHR6hOAblv0 |
|	13	| SG51R					|	2		| 2201				| https://www.mouser.de/ProductDetail/Adafruit/2201?qs=GURawfaeGuDnyC8bBxCsCw%3D%3D |
|	14	| Si2302CDS				|	6		| SI2302CDS-T1-E3	| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%2Fha2pyFaduhn77Cd8sORMCPlOQ64g5p%252BNTSOwwQoaGlZHLEpIgs66Q%3D%3D |
|	15	| Resistor 10k Ohm		|	7		| CRCW060310K0JNEBC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|	16	| Resistor 4,7k Ohm		|	4		| CRCW06034K70FKEA	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|	17	| Resistor 80 Ohm		|	5		| ERJ-3EKF80R6V		| https://www.mouser.de/ProductDetail/Panasonic/ERJ-3EKF80R6V?qs=%2Fha2pyFaduglTbPDDZSypdojfHHtTpVMCJsBXOPSqw%252B%252BQEm3Bf%2FUPA%3D%3D |
|	18	| Resistor 0,5 Ohm 1W	|	1		| PCS2512FR5000ET	| https://www.mouser.de/ProductDetail/Ohmite/PCS2512FR5000ET?qs=%2Fha2pyFaduh8JOJSEvl2lWe4t5yRBRxyUIrjOcdfxIh1nnP6l5UmJw%3D%3D |
|	19	| Resistor 75m Ohm		|	1		| CRCW060375R0FKEAC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060375R0FKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLSMOQT%252B%252BY1yeMkXcpHnt60jWRc7zaAQmIZ%2FXHNPoqn0k |
|	20	| Resistor 1k Ohm		|	2		| CRCW06031K00FKEAC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06031K00FKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLVmyhq699xnzTj0BYiFB3%2FaLM9ykHyj5ejAgw8293GI7 |
|	21	| Resistor 220k Ohm		|	1		| RC0603FR-10220KL	| https://www.mouser.de/ProductDetail/Yageo/RC0603FR-10220KL?qs=%2Fha2pyFaduhXXNW8qwNUNniVXko%2F2rso1wWz%2Fe%2FJ72XFHyhjTASuhw%3D%3D |
|	22	| Resistor 100k Ohm		|	2		| CRCW0603100KFKEAC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW0603100KFKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLcw2h53PIEbpqmPrufcnxqrQ5neTgziOAfer%252BU385wIB |
|	23	| AM-1819CA				|	2		| AM-1819CA			| https://www.mouser.de/ProductDetail/Panasonic-Battery/AM-1819CA?qs=%2Fha2pyFaduiLJSQJRZRJsZzwp1SWwKVm%252BDvNb84w%252BEtAl%2FqRz6jO0w%3D%3D |
|	24	| PTS815				|	1		| PTS815 SJM 250 SMTR LFS | https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=%2Fha2pyFaduhI5UFm74GJ4OWa%2FUrmw2fJIH2fZNLZK%2Fapflh8XMB109Sdai2%252Bj7mo |
|	25	| ESP32-WROOM-32SD		|	1		| ESP32-WROOM-32D(M113DH3200PH3Q0) | https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH3200PH3Q0?qs=%2Fha2pyFadugKGKyA%252BZerg2B0xz1dlPNH74rLf7O9OSvWVa4574SRV7OWtyNwqCoDyQsmrbbIWmE%3D |
|	26	| AP2114HA				|	1		| AP2114HA-3.3TRG1	| https://www.mouser.de/ProductDetail/Diodes-Incorporated/AP2114HA-33TRG1?qs=%2Fha2pyFaduhMNFj8Kq2MI%252BCc4uxHDDDopEFND498zqimfTkb7WQQMg%3D%3D |
|	27	| AD8400ARZ10			|	1		| AD8400ARZ10		| https://www.mouser.de/ProductDetail/Analog-Devices/AD8400ARZ10?qs=%2FtpEQrCGXCwM8Fz00jiA6Q%3D%3D |
|	28	| LT3081R				|	1		| LT3081ER#PBF		| https://www.mouser.de/ProductDetail/Analog-Devices/LT3081ERPBF?qs=hVkxg5c3xu9EsVcFcpdoiQ%3D%3D |
|	29	| INA233				|	1		| INA233AIDGSR		| https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D |
|	30	| TSL235R-LF			|	2		| TSL235R-LF		| https://www.mouser.de/ProductDetail/ams/TSL235R-LF?qs=%2Fha2pyFadugXAYQ2OeTg%252B0wbzJQDJbiJit122rjMF0g%3D |
|	31	| LM321LV				|	1		| LM321LVIDBVR		| https://www.mouser.de/ProductDetail/Texas-Instruments/LM321LVIDBVR?qs=%2Fha2pyFaduih9iarg3%252BHAWBhweN761gHozjdK09Y3pv7QCfsAo0%252BLA%3D%3D |
|	32	| TLV1805				|	1		| TLV1805DBVR		| https://www.mouser.de/ProductDetail/Texas-Instruments/TLV1805DBVR?qs=%2Fha2pyFaduiR0pAkR5SNIi%252BKs0gmLfIgpZmsXM3Thpmf733Zrve9vw%3D%3D |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/solar_farm/solar_farm.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/solar_farm/solar_farm.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/solar_farm/solar_farm.md) 