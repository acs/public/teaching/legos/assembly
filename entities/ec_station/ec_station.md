# Electric Car Station
 
## Explosion Drawing
<img src="docs/ec_station_ex.png"  width="589" height="400">
   
### 3D parts
-	Part 1: Base
-   Part 2: Roofholder
-   Part 3: Roof

### Plexiglas
-	Plexiglas 6x15x3 mm [Rect]
-	Plexiglas 66x90x3 mm [Rect]

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code	    | Distributor		    |
| ------    | ------ 				| ------    | ------                | ------    |
|   1       | Screw M1.6			|   4       |                       |           |
|   2       | RFID Shields          |   2 		| SBC-RFID-RC522        | [Shield](https://www.reichelt.de/entwicklerboards-rfid-modul-nxp-mfrc-522-debo-rfid-rc522-p192147.html)   |
|   3       | Plexiglas trn 66x90x3 mm| 1       |                       |           |
|   4       | Plexiglas wh 6x15x3 mm  | 2       |                       |           |
|   5       | SML-LX0606IGC-TR      |   2       | SML-LX0606IGC-TR      |  https://www.mouser.de/ProductDetail/Lumex/SML-LX0606IGC-TR?qs=%2Fha2pyFadui65xCOfAXPsn2teWv0QH2ptxEhf8UTblMXEUJUoof2dw%3D%3D |
|   6       | Si2302CDS             |   5       | SI2302CDS-T1-E3   	| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%252BPu8jn5UVnHNrjAmGCs%2Fuw%3D%3D |
|   7       | Capacitor 1μF         |   4       | 02016D104KAT2A        | https://www.mouser.de/ProductDetail/AVX/02016D104KAT2A?qs=PN7sAUOUrntlfwxbZzo6OQ%3D%3D |
|   8       | Resistor 140 Ohm      |   2       | AC0603FR-07140RL      | https://www.mouser.de/ProductDetail/Yageo/AC0603FR-07140RL?qs=%2Fha2pyFadujj9sgtHXfPQoi22YJL2PAZJh4980WGH09ZdWAW1uWKDQ%3D%3D |
|   9       | Resistor 150 Ohm      |   2       | CR0603-JW-151ELF      | https://www.mouser.de/ProductDetail/Bourns/CR0603-JW-151ELF?qs=%2Fha2pyFadujUyve4dPjTxKBzWuCfjvZmI65LQafJWEBCwJ0zDtOjvg%3D%3D |
|   10      | Resistor 10k Ohm      |   9       | CRCW060310K0JNEBC     | https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|   11      | Resistor 4,7k Ohm     |   5       | CRCW06034K70FKEA      | https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|   12      | Resistor 100m Ohm     |   1       | KDV12DR100ET          | https://www.mouser.de/ProductDetail/Ohmite/KDV12DR100ET?qs=%2Fha2pyFadujYEP9Yjf896LY%252BB6oL%2FcSG2HG0xFKEjpbehyNOlB0Tog%3D%3D |
|   13      | Resistor 100k Ohm     |   1       | CRCW0603100KFKEAC     | https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW0603100KFKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLcw2h53PIEbpqmPrufcnxqrQ5neTgziOAfer%252BU385wIB |
|   14      | Resistor 30 Ohm 1/2W  |   1       | ERJ-P06F30R0V         | https://www.mouser.de/ProductDetail/Panasonic/ERJ-P06F30R0V?qs=%2Fha2pyFadug2IR%252BKGI4fm4k6Nc2Wd3hhSuw44PMyKeWZPTHQUFzQWA%3D%3D |
|   15      | INA233                |   1       | INA233AIDGSR          | https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D  |
|   16      | Capacitor 100nF       |   4       | 06033C104KAT4A        | https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|   17      | Capacitor 10μF        |   2       | GRM188R60J106ME84J    | https://www.mouser.de/ProductDetail/Murata-Electronics/GRM188R60J106ME84J?qs=jHkklCh7amjgoxFXBHHBCA%3D%3D |
|   19      | TLV333IDBV            |   1       | TLV333IDBVR           | https://www.mouser.de/ProductDetail/Texas-Instruments/TLV333IDBVR?qs=n%252BTO0c4TDSbTbn2suQLlAQ%3D%3D |
|   20      | TLV803E               |   1       | TLV803EA29DBZR        | https://www.mouser.de/ProductDetail/Texas-Instruments/TLV803EA26DPWR?qs=W%2FMpXkg%252BdQ7k%252BxQuvIufAw%3D%3D  |
|   21      | PTS815                |   1       | PTS815 SJM 250 SMTR LFS | https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=ahcBuItHZ3xKWmfV%2F2E6bA==    |
|   22      | ESP32                 |   1       | ESP32-WROOM-32D(M113DH3200PH3Q0) | https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH2800PH3Q0?qs=sGAEpiMZZMu3sxpa5v1qruuGqP4jgT%2FvnmTo7KFPAbE%3D |
|   23      | BAT60A                |   1       | BAT60AE6327HTSA1      | https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D |
|   24      | MountinHole           |   6       | 97730356332R          | https://www.mouser.de/ProductDetail/Wurth-Elektronik/97730356332R?qs=%2Fha2pyFaduhh7j50G6IZ4FwQQt1JrBnGUNliCy6ozjs4NQ5%2FL59kQA%3D%3D |
|   25      | Connector PROG        |   1       | 20021511-00006T4LF    | https://www.mouser.de/ProductDetail/Amphenol-FCI/20021511-00006T4LF?qs=%2Fha2pyFaduj%2Fipt9xra2X7w9g1wuSqbzBLkT1LceF%2FnU8fY47ySEttXVb41GLTyl |
|   26      | Connector Pogo        |   10      | 0908-9-15-20-75-14-11-0 | https://www.mouser.de/ProductDetail/Mill-Max/0908-9-15-20-75-14-11-0?qs=%2Fha2pyFaduhtbXauCNY5uTRkR8EFNEPBXMOO3bv7XLa7djLJN1kijCHR6hOAblv0 |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/ec_station/ec_station.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/ec_station/ec_station.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/ec_station/ec_station.md) 