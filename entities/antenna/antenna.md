# Antenna

## Explosion Drawing
<img src="docs/antenna_ex.png"  width="196" height="400">

### 3D parts
-	Part 1: Base
-   Part 2:	Walls
-   Part 3: Roof
-   Part 4: Pillar
-   Part 5: Dipoles

### Plexiglas
-	Plexiglas 62x55x3 mm [Rect]
-	Plexiglas 89x55x3 mm [Rect]

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |
|-----  | ------ 					| ------ 	| ------	    		  | ------			|
|   1   | RaspberryPi 3B+      		|  1      	|   				 	  |  				|
|	2	| Plexiglas trn 62x55x3 mm	|  1 		|						  |					|
|	3	| Plexiglas trn 89x55x3 mm	|  1 		|						  |					|
|	4	| Srew M1.6					|  7		| 						  |					|
|	5	| Capacitor  22 µF			|  1		| CC0603MRX6S5BB226		  | https://www.mouser.de/ProductDetail/Yageo/CC0603MRX6S5BB226?qs=%2Fha2pyFaduhl02ZdSmx%252BYg0lz62tyouIvbUB3OrGFSj9Ivgtle%2FrUACvrKWfQAIx |
|	6	| Capacitor 100 nF			|  1		| 06033C104KAT4A		  | https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|	7	| LED						|  4		| MT7315B-UR-A			  | https://www.digikey.de/products/de?keywords=MT7315B-UR-A |
|	8	| MountingHole				|  6		| 97730356332R			  | https://www.mouser.de/ProductDetail/Wurth-Elektronik/97730356332R?qs=%2Fha2pyFaduhh7j50G6IZ4FwQQt1JrBnGUNliCy6ozjs4NQ5%2FL59kQA%3D%3D |
|	9	| Connector Pogo			| 10		| 0908-9-15-20-75-14-11-0 | https://www.mouser.de/ProductDetail/Mill-Max/0908-9-15-20-75-14-11-0?qs=%2Fha2pyFaduhtbXauCNY5uTRkR8EFNEPBXMOO3bv7XLa7djLJN1kijCHR6hOAblv0 |
|	10	| Connector 01x08			|  1		| SSM-104-F-DV			  | https://www.mouser.de/ProductDetail/Samtec/SSM-104-F-DV?qs=rU5fayqh%252BE1VcPc15WZEOQ%3D%3D |
|	11	| MMBT3906					|  1		| MMBT3906				  | https://www.mouser.de/ProductDetail/Rectron/MMBT3906?qs=w%2Fv1CP2dgqpljHf4BP5MNw%3D%3D |
|	12	| Si2301BDS					|  1		| SI2301BDS-T1-E3		  | https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2301BDS-T1-E3?qs=%2Fha2pyFaduj3XLid0RWuwc6M6sr4X6TDa51gm78P1G9joMb%252BNZQ9ww%3D%3D |
|	13	| Si2302CDS					|  4		| SI2302CDS-T1-E3		  | https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%2Fha2pyFaduhn77Cd8sORMCPlOQ64g5p%252BNTSOwwQoaGlZHLEpIgs66Q%3D%3D |
|	14	| MMBT3904					|  1		| MMBT3904				  | https://www.mouser.de/ProductDetail/Rectron/MMBT3904?qs=w%2Fv1CP2dgqqR04yFya3A3w%3D%3D |
|	15	| Resistor 100 kOhm			|  1		| CRCW0603100KFKEAC		  | https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW0603100KFKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLcw2h53PIEbpqmPrufcnxqrQ5neTgziOAfer%252BU385wIB |
|	16	| Resistor   1 kOhm			|  1		| CRCW06031K00FKEAC		  | https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06031K00FKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLVmyhq699xnzTj0BYiFB3%2FaLM9ykHyj5ejAgw8293GI7 |
|	17	| Resistor  50 mOhm			|  1		| LVK12R050CER			  | https://www.mouser.de/ProductDetail/Ohmite/LVK12R050CER?qs=%2Fha2pyFaduj7XFfIkG4bGop58u%2Fg9RC6%252Bnv9AX2esgFqkJqz4lozxw%3D%3D |
|	18	| Resistor 4,7 kOhm			|  4		| CRCW06034K70FKEA		  | https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|	19	| Resistor  68 Ohm			|  4		| CR0603-FX-68R0ELF		  | https://www.mouser.de/ProductDetail/Bourns/CR0603-FX-68R0ELF?qs=%2Fha2pyFadugVmr2gairtNy33M8%252BD61rGuWVNQzjAYmtx%252BQ1gsxBA2Q%3D%3D |
|	20	| PTS815					|  1		| PTS815 SJM 250 SMTR LFS | https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=%2Fha2pyFaduhI5UFm74GJ4OWa%2FUrmw2fJIH2fZNLZK%2Fapflh8XMB109Sdai2%252Bj7mo |
|	21	| INA233					|  1		| INA233AIDGSR			  | https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D |


## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/antenna/antenna.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/antenna/antenna.md)
* [Software]() 
