# Factory
## Explosion Drawing
<img src="docs/factory_ex.png"  width="270" height="400">

### 3D parts
-	Part 1: Base
-	Part 2: Servo cover
-	Part 3: Walls
-	Part 4: Roof
-	Part 5: Chimney
-	Part 6: Robotic Arm

### Plexiglas
-	Plexiglas 50x78x3 mm [Rect]
-	Plexiglas 50x74x3 mm [Rect]
-	Plexiglas 78x78x3 mm [Triang]

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws
-	Magnets

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |
|-----  | ------ 					| ------ 	| ------				| ------	|
|	1	| Screw M1.6				|	6		| 						| 			|
|	2	| Magnets 3x3x0.5mm		 	|	5  		| 						| https://www.neomagnete.de/de/quadermagnet-3x3x0-5mm-neodym-n48?c=6|
|   3   | Plexiglas trn 50x78x3 mm 	|  	1   	|						|			|
|   4   | Plexiglas trn 50x74x3 mm 	|  	1   	|						|			|
|   5   | Plexiglas trn 78x78x3 mm 	|  	1   	|						|			|
|	6	| esp32						|	1		| ESP32-WROOM-32D(M113DH3200PH3Q0) | https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH2800PH3Q0?qs=sGAEpiMZZMu3sxpa5v1qruuGqP4jgT%2FvnmTo7KFPAbE= |
|	7	| LED (IN-S126AT5UW)		|	5		| IN-S126AT5UW			| https://www.mouser.de/ProductDetail/Inolux/IN-S126AT5UW?qs=%2Fha2pyFadujcmMPeskJ8Vctm2Py%2Ft%2Frfmi0kzFIvJQCC2eq40OLaYw%3D%3D |
|	8	| Resistor 80 Ohm			|	5		| ERJ-3EKF80R6V			| https://www.mouser.de/ProductDetail/Panasonic/ERJ-3EKF80R6V?qs=%2Fha2pyFaduglTbPDDZSypdojfHHtTpVMCJsBXOPSqw%252B%252BQEm3Bf%2FUPA%3D%3D |
|	9	| Resistor 10k Ohm			|	11		| CRCW060310K0JNEBC		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|	10	| Resistor 4,7k Ohm			|	4		| CRCW06034K70FKEA		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|	11	| Resistor 100m Ohm			|	1		| KDV12DR100ET			| https://www.mouser.de/ProductDetail/Ohmite/KDV12DR100ET?qs=%2Fha2pyFadujYEP9Yjf896LY%252BB6oL%2FcSG2HG0xFKEjpbehyNOlB0Tog%3D%3D |
|	12	| Resistor 2,4 Ohm 1/4W		|	1		| RK73B2ATTD2R4J		| https://www.mouser.de/ProductDetail/KOA-Speer/RK73B2ATTD2R4J?qs=%2Fha2pyFaduhcnQkibzv07swb%2F4ICrOym%2FSlU917bcVShWkLNoQt3Ig%3D%3D |
|	13	| Resistor 3,6 Ohm 1/4W		|	1		| RK73B2ATTD3R6J 		| https://www.mouser.de/ProductDetail/KOA-Speer/RK73B2ATTD3R6J?qs=%2Fha2pyFaduhcnQkibzv07oQ5hGPPfx97hBdMoJ0k55ydmOrHZIleiQ%3D%3D |
|	14	| Capacitor 100μF			|	2		| TLJA107M004R0500		| https://www.mouser.de/ProductDetail/AVX/TLJA107M004R0500?qs=%2Fha2pyFadujTuboaZwHWz7DloL6LFca9nKUaIEFzt%252BCb5bPSnYBQmw%3D%3D |
|	15	| Capacitor 100nF			|	2		| 06033C104KAT4A		| https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|	16	| Capacitor 10μF 			|	2		| GRM188R60J106ME84J	| https://www.mouser.de/ProductDetail/Murata-Electronics/GRM188R60J106ME84J?qs=jHkklCh7amjgoxFXBHHBCA%3D%3D |
|	17	| BAT60A					|	1		| BAT60AE6327HTSA1		| https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D |
|	18	| INA233					|	1		| INA233AIDGSR			| https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D |
|	19	| SG51R						|	1		| 2201					| https://www.mouser.de/ProductDetail/Adafruit/2201?qs=GURawfaeGuDnyC8bBxCsCw%3D%3D |
|	20	| Si2302CDS					|	8		| SI2302CDS-T1-E3		| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%252BPu8jn5UVnHNrjAmGCs%2Fuw%3D%3D |
|	21	| TLV803E					|	1		| TLV803EA29DBZR		| https://www.mouser.de/ProductDetail/Texas-Instruments/TLV803EA26DPWR?qs=W%2FMpXkg%252BdQ7k%252BxQuvIufAw%3D%3D |
|	22	| PTS815					|	1		| PTS815 SJM 250 SMTR LFS | https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=ahcBuItHZ3xKWmfV%2F2E6bA%3D%3D |
|	23	| L128						|	2		| L128-RYL1003500000	| https://www.mouser.de/ProductDetail/Lumileds/L128-RYL1003500000?qs=%2Fha2pyFadujbsfoG%2FzT6JpPzDTeptfcYzI%2F%2F8euKQxAiY8jA20gEhWoTmpY%2FZ2Fl |
|	24	| MountingHole				|	6		| 97730356332R			| https://www.mouser.de/ProductDetail/Wurth-Elektronik/97730356332R?qs=%2Fha2pyFaduhh7j50G6IZ4FwQQt1JrBnGUNliCy6ozjs4NQ5%2FL59kQA%3D%3D |
|	25	| Connector PROG			|	1		| 20021511-00006T4LF	| https://www.mouser.de/ProductDetail/Amphenol-FCI/20021511-00006T4LF?qs=%2Fha2pyFaduj%2Fipt9xra2X7w9g1wuSqbzBLkT1LceF%2FnU8fY47ySEttXVb41GLTyl |
|	26	| Connector Pogo			|	10		| 0908-9-15-20-75-14-11-0 | https://www.mouser.de/ProductDetail/Mill-Max/0908-9-15-20-75-14-11-0?qs=%2Fha2pyFaduhtbXauCNY5uTRkR8EFNEPBXMOO3bv7XLa7djLJN1kijCHR6hOAblv0 |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/factory/factory.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/factory/factory.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/factory/factory.md) 