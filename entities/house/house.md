# House

## Explosion Drawing
<img src="docs/house_ex.png"  width="334" height="400">

### 3D parts
-	Part 1: Base
-	Part 2: Walls
-	Part 3: Fan support
-	Part 4: Kitchen
-	Part 5: Roof

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws
-	Magnets

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |
|   1   | SolarCell 	        |   1       | AM-5412CAR		| https://www.mouser.de/ProductDetail/Panasonic-Battery/AM-5412CAR?qs=79dOc3%2F91%2Ffye9jVGVG7AQ%3D%3D |
|   2   | Plexiglas trn 96x60x3 mm| 2       |               	|				|
|   3   | BAT60A                |   3    	| BAT60AE6327HTSA1	| https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D |
|   4   | Resistor 4,7k Ohm     |   9       | CRCW06034K70FKEA 	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|   5   | Resistor 100m Ohm		|   2    	| KDV12DR100ET		| https://www.mouser.de/ProductDetail/Ohmite/KDV12DR100ET?qs=%2Fha2pyFadujYEP9Yjf896LY%252BB6oL%2FcSG2HG0xFKEjpbehyNOlB0Tog%3D%3D |
|   6   | Resistor 2,49 Ohm 400mW | 1    	| RCS08052R49FKEA	| https://www.mouser.de/ProductDetail/Vishay-Dale/RCS08052R49FKEA?qs=%2Fha2pyFaduisEDCP2eoct%252B0CaZBEsZCVa4JEC3AMYDTYHrRlumr04g%3D%3D |
|   7   | Resistor 10k Ohm      |   10    	| CRCW060310K0JNEBC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|   8   | Resistor 1k Ohm       |   2       | CRCW06031K00FKEAC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06031K00FKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLVmyhq699xnzTj0BYiFB3%2FaLM9ykHyj5ejAgw8293GI7 |
|   9   | Resistor 330 Ohm      |   22      | CRCW0603330RJNEAC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW0603330RJNEAC?qs=%2Fha2pyFaduhQpfMWkxpuLWM32NMZehgZudmYL9BiHe12vCOHqB4%252BuBi1BTg6IUYe |
|   10  | Resistor 100k Ohm     |   2       | CRCW0603100KFKEAC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW0603100KFKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLcw2h53PIEbpqmPrufcnxqrQ5neTgziOAfer%252BU385wIB |
|   11  | Resistor 100 Ohm      |   1       | CRCW0603100RFKEAC	| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW0603100RFKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLXlkDUXKZxW6R4mojuBr2YWpEWpG3X8Xg%252Bp2it%2F3qp%252BA |
|   12  | Resistor 120 Ohm      |   4       | RC0603JR-7W120RL	| https://www.mouser.de/ProductDetail/Yageo/RC0603JR-7W120RL?qs=%2Fha2pyFaduiI%252B%2F%2Fp%2FYhTTYcnC%252BHD46zBj1j1sJh3MoaRQ8BEO%252B8qTw%3D%3D |
|   13  | Resistor 180 Ohm      |   25      | ERJ-3EKF1800V		| https://www.mouser.de/ProductDetail/Panasonic/ERJ-3EKF1800V?qs=%2Fha2pyFaduglTbPDDZSypfTAdn6P%252BL1Urg0aUURqOm%252Bm49twFs0qdg%3D%3D |
|   14  | Resistor 270 Ohm      |   3       | CR0603-FX-2700ELF	| https://www.mouser.de/ProductDetail/Bourns/CR0603-FX-2700ELF?qs=%2Fha2pyFadugVmr2gairtN3z2lVSjqxGpk9y7OUPVmJj23Tn3y6NsjA%3D%3D |
|   15  | Capacitor 10μF        |   2       | 0603ZD106KAT2A	| https://www.mouser.de/ProductDetail/AVX/0603ZD106KAT2A?qs=c1aAEamvUYHQhwHZOF6y0Q%3D%3D |
|   16  | Capacitor 100nF       |   5       | 06033C104KAT4A	| https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|   17  | Capacitor 220pF       |   1       | 06033A221JAT2A	| https://www.mouser.de/ProductDetail/AVX/06033A221JAT2A?qs=icS7CTEoV1ML9Af1nc8Zaw%3D%3D |
|   18  | Capacitor 100μF       |   2       | TLJA107M004R0500	| https://www.mouser.de/ProductDetail/AVX/TLJA107M004R0500?qs=%2Fha2pyFadujTuboaZwHWz7DloL6LFca9nKUaIEFzt%252BCb5bPSnYBQmw%3D%3D |
|   19  | Capacitor 30pF        |   1       | 06035A300JAT2A	| https://www.mouser.de/ProductDetail/AVX/06035A300JAT2A?qs=yuhjMS%2FIYxYp690hzqRzIg%3D%3D |
|	20	| Capacitor 10nF		|	2		| 06035C103JAT2A	| https://www.mouser.de/ProductDetail/AVX/06035C103JAT2A?qs=aquwThhUVI9dzXPTvGj7jQ%3D%3D |	
|   21  | esp32                 |   1       | ESP32-WROOM-32D(M113DH3200PH3Q0) | https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH3200PH3Q0?qs=%2Fha2pyFadugKGKyA%252BZerg2B0xz1dlPNH74rLf7O9OSvWVa4574SRV7OWtyNwqCoDyQsmrbbIWmE%3D |
|   22  | PTS815                |   1       | PTS815 SJM 250 SMTR LFS | https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=ahcBuItHZ3xKWmfV%2F2E6bA%3D%3D |
|   23  | LM321LV               |   1       | LM321LVIDBVR		| https://www.mouser.de/ProductDetail/Texas-Instruments/LM321LVIDCKR?qs=qSfuJ%252Bfl%2Fd6RcyTOjkRx5Q%3D%3D|
|   24  | INA233                |   2       | INA233AIDGSR		| https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D |
|   25  | AD8400ARZ10           |   1       | AD8400ARZ10		| https://www.mouser.de/ProductDetail/Analog-Devices/AD8400ARZ10?qs=%2FtpEQrCGXCwM8Fz00jiA6Q%3D%3D |
|   26  | LT3081R               |   1       | LT3081ER#PBF		| https://www.mouser.de/ProductDetail/Analog-Devices/LT3081ERPBF?qs=hVkxg5c3xu9EsVcFcpdoiQ%3D%3D |
|   27  | Si2302CDS             |   14      | SI2302CDS-T1-E3	| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%252BPu8jn5UVnHNrjAmGCs%2Fuw%3D%3D |
|   28  | CD74HC4511PW          |   1       | CD74HC4511PWRE4	| https://www.mouser.de/ProductDetail/Texas-Instruments/CD74HC4511PWRE4?qs=xFfolx0DHx1qW9PAIFxsTQ%3D%3D |
|   29  | Si2301BDS             |   9       | SI2301BDS-T1-E3	| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2301BDS-T1-GE3?qs=tWM4QuTcS4VUQRxq4xvp7w%3D%3D |
|   30  | CP20351               |   1       | CP20351			| https://www.mouser.de/ProductDetail/CUI-Devices/CP20351?qs=%2Fha2pyFadugwdQb86RJtLElciDZaWOmOBn4erSV7%252BgA%3D |
|   31  | SN74AUP1G32DBV        |   1       | SN74AUP1G32DBVR	| https://www.mouser.de/ProductDetail/Texas-Instruments/SN74AUP1G32DBVR?qs=iO7mgM19OaZv5mY3EPipeg%3D%3D |
|   32  | TMP100                |   2       | TMP100AQDBVRQ1	| https://www.mouser.de/ProductDetail/Texas-Instruments/TMP100AQDBVRQ1?qs=5aG0NVq1C4xMfyLhtOUNYQ%3D%3D |
|   33  | PCA9536D              |   1       | PCA9536D			| https://www.mouser.de/ProductDetail/Texas-Instruments/PCA9536D?qs=aEuGZpxfbxW%2FOd%252B%2FWDyXUw%3D%3D |
|   34  | IN-S126ATR            |   7       | IN-S126ATR		| https://www.mouser.de/ProductDetail/Inolux/IN-S126ATR?qs=%2Fha2pyFadujcmMPeskJ8VRjXpiffnQ0ty1RGvy2OxlnlSpFGSOpZkw%3D%3D |
|   35  | IN-S126ATB            |   17      | IN-S126ATB		| https://www.mouser.de/ProductDetail/Inolux/IN-S126ATB?qs=%2Fha2pyFadujcmMPeskJ8VasOrrwMh3b8NprSh8TiU4cGDhSVmNv5sA%3D%3D |
|   36  | IN-S126AT5UW          |   8       | IN-S126AT5UW		| https://www.mouser.de/ProductDetail/Inolux/IN-S126AT5UW?qs=%2Fha2pyFadujcmMPeskJ8Vctm2Py%2Ft%2Frfmi0kzFIvJQCC2eq40OLaYw%3D%3D |
|   37  |Pelitier  	            |   1     	| 430005-518		| https://www.digikey.de/product-detail/de/430005-518/430005-518-ND/10059127?utm_campaign=buynow&WT.z_cid=ref_octopart_dkc_buynow&utm_medium=aggregator&curr=eur&site=us&utm_source=octopart |
|	38	| 7 Segment Display	    |   3       | ACSA03-41SURKWA-F01 | https://www.mouser.de/ProductDetail/Kingbright/ACSA03-41SURKWA-F01?qs=%2Fha2pyFadugsEfitBMBBYru2dtkrH%2FnNMoDC6kKUHr1%2FGnq3HbHVbKT27m5mOfT1 |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/house/house.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/house/house.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/house/house.md) 