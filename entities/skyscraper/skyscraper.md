# Skyscraper

## Explosion Drawing
<img src="docs/skyscraper_ex.png"  width="243" height="400">

### 3D parts
-	Part 1: Base
-	Part 2: Building
-	Part 3: Roof
-	Part 4: Flap
-	Part 5: Furniture

### Connecting Parts 
-   Epoxy glue
-   M1.6 screws
-	Magnets

## Bill of materials
|Part   | Component 				| Count 	| Manufacturer code		  | Distributor		    |		
|	1	| Capacitor 10µF		|	3		| GRM188R60J106ME84J	| https://www.mouser.de/ProductDetail/Murata-Electronics/GRM188R60J106ME84J?qs=jHkklCh7amjgoxFXBHHBCA%3D%3D |
|	2	| Capacitor 100nF		|	2		| 06033C104KAT4A		| https://www.mouser.de/ProductDetail/AVX/06033C104KAT4A?qs=8C2chATdSPiv3E9zfPZulg%3D%3D |
|	3	| SSL-LX2573SGD			|	1		| SSL-LX2573SGD			| https://www.mouser.de/ProductDetail/Lumex/SSL-LX2573SGD?qs=%2Fha2pyFaduhRWKmuGRnUlTDnuxHznOprbKwcMWXexIQhIKSfSEhibw%3D%3D |
|	4	| BAT60A				|	1		| BAT60AE6327HTSA1		| https://www.mouser.de/ProductDetail/Infineon-Technologies/BAT60AE6327HTSA1?qs=OwbwYO03UsIW3Y2lCXbs8Q%3D%3D |
|	5	| IN-S124ARB			|	3		| IN-S124ARB			| https://www.mouser.de/ProductDetail/Inolux/IN-S124ARB?qs=%2Fha2pyFaduhFrQeiBMucr5AizyyVuM2b6OqO5tvfddYBOyKpFqAldw%3D%3D |
|	6	| OVLLY8C7				|	12		| OVLLY8C7				| https://www.mouser.de/ProductDetail/Optek-TT-Electronics/OVLLY8C7?qs=%2Fha2pyFadugSjzMOsY7ykHd6Xuyo1%252BWU9YR49eVTlOU%3D |
|	7	| C513A-WSS-CW0Z0151	|	3		| C513A-WSS-CW0Z0151	| https://www.mouser.de/ProductDetail/Cree-Inc/C513A-WSS-CW0Z0151?qs=%2Fha2pyFadugnAEMUPTKq0dkiqwq4xiTAwGxBpYJVELWQJocgJYxUdq2nKVntJWzI |
|	8	| WP1503SRD				|	2		| WP1503SRD				| https://www.mouser.de/ProductDetail/Kingbright/WP1503SRD?qs=%2Fha2pyFaduiSlOU85NJAEiMiVMZf30b%252Bm%252Bse4ElCQIg%3D |
|	9	| VAOL-5701SBY4			|	1		| VAOL-5701SBY4			| https://www.mouser.de/ProductDetail/VCC/VAOL-5701SBY4?qs=%2Fha2pyFaduhlFvMQppelmHt8Ve4eBYm1%252BBMVgFAhVLpX6EwUg6GvJA%3D%3D |
|	10	| MountingHole			|	6		| 97730356332R			| https://www.mouser.de/ProductDetail/Wurth-Elektronik/97730356332R?qs=%2Fha2pyFaduhh7j50G6IZ4FwQQt1JrBnGUNliCy6ozjs4NQ5%2FL59kQA%3D%3D |
|	11	| Connector 01x04		|	1		| SSM-104-L-SV-TR		| https://www.mouser.de/ProductDetail/Samtec/SSM-104-L-SV-TR?qs=%2Fha2pyFaduig6BbXSaFvdbBs5neKT8jFVRceTCUzSOqrv6YccH2BWA%3D%3D |
|	12	| Connector Prog		|	1		| 20021511-00006T4LF	| https://www.mouser.de/ProductDetail/Amphenol-FCI/20021511-00006T4LF?qs=%2Fha2pyFaduj%2Fipt9xra2X7w9g1wuSqbzBLkT1LceF%2FnU8fY47ySEttXVb41GLTyl |
|	13	| Connector Pogo		|	10		| 0908-9-15-20-75-14-11-0| https://www.mouser.de/ProductDetail/Mill-Max/0908-9-15-20-75-14-11-0?qs=%2Fha2pyFaduhtbXauCNY5uTRkR8EFNEPBXMOO3bv7XLa7djLJN1kijCHR6hOAblv0 |
|	14	| Connector 01x02		|	1		| M50-3140245			| https://www.mouser.de/ProductDetail/Harwin/M50-3140245?qs=%252BdQmOuGyFcEdctCQD%252BcFMQ%3D%3D |
|	15	| Connector 01x03		|	4		|					|  |
|	16	| SG51R					|	1		| 2201					| https://www.mouser.de/ProductDetail/Adafruit/2201?qs=GURawfaeGuDnyC8bBxCsCw%3D%3D |
|	17	| Fan_IN				|	1		| MF30060V2-1000U-A99	| https://www.mouser.de/ProductDetail/Sunon/MF30060V2-1000U-A99?qs=%2Fha2pyFaduhpAG16HYh3rNNAzrD0c%252BQH2zKIfVreRxR0jF7BnOGN9jjI2JE6CrNE |
|	18	| Si2302CDS				|	14		| SI2302CDS-T1-E3		| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2302CDS-T1-E3?qs=%2Fha2pyFaduhn77Cd8sORMCPlOQ64g5p%252BNTSOwwQoaGlZHLEpIgs66Q%3D%3D |
|	19	| Si2301BDS				|	1		| SI2301BDS-T1-E3		| https://www.mouser.de/ProductDetail/Vishay-Semiconductors/SI2301BDS-T1-E3?qs=%2Fha2pyFaduj3XLid0RWuwc6M6sr4X6TDa51gm78P1G9joMb%252BNZQ9ww%3D%3D |
|	20	| Resistor 10k Ohm		|	18		| CRCW060310K0JNEBC		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060310K0JNEBC?qs=%2Fha2pyFaduhQpfMWkxpuLRXRKQ6TRfPb6vtyuv%252BxABTKzFRXn3X4A%2F2Cm%2FgQixNj |
|	21	| Resistor 55 Ohm		|	1		| CR0603-FX-54R9ELF		| https://www.mouser.de/ProductDetail/Bourns/CR0603-FX-54R9ELF?qs=%2Fha2pyFadugVmr2gairtNx%252BDgsFdi8q%252B0iingIuiJHISeKJlB4SZlg%3D%3D |
|	22	| Resistor 4,7k Ohm		|	4		| CRCW06034K70FKEA		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW06034K70FKEA?qs=4E3O%252BEEZcdLDG%2FSk4GmlWQ%3D%3D |
|	23	| Resistor 100m Ohm		|	1		| KDV12DR100ET			| https://www.mouser.de/ProductDetail/Ohmite/KDV12DR100ET?qs=%2Fha2pyFadujYEP9Yjf896LY%252BB6oL%2FcSG2HG0xFKEjpbehyNOlB0Tog%3D%3D |
|	24	| Resistor 20 Ohm		|	4		| SDR03EZPJ200			| https://www.mouser.de/ProductDetail/ROHM-Semiconductor/SDR03EZPJ200?qs=%2Fha2pyFaduj8ju4lCAzuYqXVu6SzA0hARb0ZqeVpcHd67TMFnSadeA%3D%3D |
|	25	| Resistor 60 Ohm		|	13		| RC0603FR-0760R4L		| https://www.mouser.de/ProductDetail/Yageo/RC0603FR-0760R4L?qs=%2Fha2pyFaduhXXNW8qwNUNqA7MGXAPcBUwj63%252B0VNoMZLVSRYJ4Sxag%3D%3D |
|	26	| Resistor 125 Ohm		|	1		| ERJ-3EKF1240V			| https://www.mouser.de/ProductDetail/Panasonic/ERJ-3EKF1240V?qs=%2Fha2pyFaduglTbPDDZSypQPpA%2Ft9akgpsezL2sykwL%2F5%2F7Kb9uohlQ%3D%3D |
|	27	| Resistor 5 Ohm		|	3		| ERJ-PA3J5R1V			| https://www.mouser.de/ProductDetail/Panasonic/ERJ-PA3J5R1V?qs=%2Fha2pyFadujfhmhVngkC6%2FRsbeEP6wTrJpwDYwLerY7IZ%2FOh2vpf8A%3D%3D |
|	28	| Resistor 30,1 Ohm		|	1		| MBA02040C3019FC100 	| https://www.mouser.de/ProductDetail/Vishay-Beyschlag/MBA02040C3019FC100?qs=%2Fha2pyFaduhcxC7X%2F90Xt1kq26PRgb%252BlqPAJ4GbtZW14Udo1GzunxQ%3D%3D |
|	29	| Resistor 75 Ohm		|	3		| CRCW060375R0FKEAC		| https://www.mouser.de/ProductDetail/Vishay-Dale/CRCW060375R0FKEAC?qs=%2Fha2pyFaduhQpfMWkxpuLSMOQT%252B%252BY1yeMkXcpHnt60jWRc7zaAQmIZ%2FXHNPoqn0k |
|	30	| PTS815				|	1		| PTS815 SJM 250 SMTR LFS | https://www.mouser.de/ProductDetail/CK/PTS815-SJM-250-SMTR-LFS?qs=%2Fha2pyFaduhI5UFm74GJ4OWa%2FUrmw2fJIH2fZNLZK%2Fapflh8XMB109Sdai2%252Bj7mo |
|	31	| TLV803E				|	1		| TLV803EA29DBZR		| https://www.mouser.de/ProductDetail/Texas-Instruments/TLV803EA29DBZR?qs=%2Fha2pyFaduhy7%252BEaq%2FQefMIt2a37GDy09phmfYeDkgETVdfWuSzdnQ%3D%3D |
|	32	| ESP32-WROOM-32D		|	1		| ESP32-WROOM-32D(M113DH3200PH3Q0) | https://www.mouser.de/ProductDetail/Espressif-Systems/ESP32-WROOM-32DM113DH3200PH3Q0?qs=%2Fha2pyFadugKGKyA%252BZerg2B0xz1dlPNH74rLf7O9OSvWVa4574SRV7OWtyNwqCoDyQsmrbbIWmE%3D |
|	33	| INA233				|	1		| INA233AIDGSR			| https://www.mouser.de/ProductDetail/Texas-Instruments/INA233AIDGSR?qs=5aG0NVq1C4wxBk2d6Xxe4A%3D%3D |
|	34	| AmbiMate				|	1		| 2316851-2				| https://www.mouser.de/ProductDetail/TE-Connectivity/2316851-2?qs=j%252B1pi9TdxUb2uPgXnnh%2Ftg%3D%3D |
|	35	| SLW-105-01-G-S		|	1		| SLW-105-01-G-S		| https://www.mouser.de/ProductDetail/Samtec/SLW-105-01-G-S?qs=%2Fha2pyFadujWQ9tj7vA4qEBVWduwJ38WBU4mc600vChWtwTl13HFNA%3D%3D |
|	36	| TLW-105-06-G-S		|	1		| TLW-105-06-G-S		| https://www.mouser.de/ProductDetail/Samtec/TLW-105-06-G-S?qs=FESYatJ8odK%2Fho610Rnlxw%3D%3D |
|	37	| 2350529-1				|	1		| 2350529-1				| https://www.mouser.de/ProductDetail/TE-Connectivity/2350529-1?qs=%2Fha2pyFadujNttRlL4mAY1H%252B5bklJr8saLMpydpvrrR%252BNGKbldCltw%3D%3D |

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/skyscraper/skyscraper.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/skyscraper/skyscraper.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/skyscraper/skyscraper.md) 