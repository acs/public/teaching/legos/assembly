# ![LEGOS](docs/legos_logo.png) <br/> Lite Emulator of Grid Operations

## Assembly
The 3D models used in [**LEGOS**](https://git.rwth-aachen.de/acs/public/teaching/legos/concept) platform for implementing the building blocks of the [**Distribution layer**](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/distribution/distribution_layer.md) and the entities of the [**Application layer**](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/application_layer.md) were designed using the free, online 3D modeling program [**Tinkercad**](http://www.tinkercad.com/).

<img src="docs/overview.png"  width="1200" height="474">

## Repository structure
The 3D models sources can be accessed freely and modified directly online in *Overview* mode, which provides an overall view of LEGOS suitable for visualizing different scenarios, or in *Models* mode, which provides all the entities arranged and ready to be individually modified.
-   [**Overview**](https://www.tinkercad.com/things/aqtpwMac7bH)
-   [**Models**](https://www.tinkercad.com/things/kJNfjDBrJ9L)

Design files for each LEGOS entity are also directly available ready for 3D printing in STL format and arranged in separate project folders:

- **entities** [*\[mechanical description\]*](entities/entities.md)
    - [antenna](entities/antenna/antenna.md): base transceiver station (BTS), provides cellular network connectivity and edge-cloud services
    - [data_center](entities/data_center/data_center.md): power critical infrastructure, essential in a cloud architecture; access to stored sensitive data triggers a data breach alert; uses a UPS in case of power sag/outage
    - [ec_station](entities/ec_station/ec_station.md): electric-car charging station; absorbs power from the grid according to custom charging profiles stored in the vehicle
    - [factory](entities/factory/factory.md): common electrical load due to manufacturing activities; absorb power to operate industrial machinery like robotic arms
    - [hospital](entities/hospital/hospital.md): power critical infrastructure, essential for healthcare services; the alert status is activated by the presence of service vehicles; uses a UPS in case of power sag/outage
    - [house](entities/house/house.md): common residential electrical load, uses heating and air conditioning for indoor temperature regulation and integrates photovoltaic cells power generation from sunlight
    - [power_plant](entities/power_plant/power_plant.md): traditional fossil-fuel power plant, provides up to 60% of the total power and controls the voltage of the grid
    - [skyscraper](entities/skyscraper/skyscraper.md): aggregated residential and commercial building; integrates a HVAC system for providing thermal comfort and acceptable indoor air quality; hosts a SCADA center for grid monitoring and for cyber-security
    - [solar_farm](entities/solar_farm/solar_farm.md): renewable energy power plant; injects current in the grid proportionally to the intensity of the incident light, optimized via 1-axis solar tracker
    - [stadium](entities/stadium/stadium.md): power hungry entity occasionally hosts football matches; significant power absorbed in case of recreational events
    - [substation](entities/substation/substation.md): essential element for the automation of an electrical grid, provides monitoring and protection functions; trips the reclosers in case of fault
    - [supermarket](entities/supermarket/supermarket.md): common electrical load due to commercial activities, integrates a CHP system for combined electrical and thermal energy generation from gas
    - [wind_farm](entities/wind_farm/wind_farm.md): renewable energy power plant; injects current in the grid proportionally to the wind intensity
- **grid** [*\[mechanical description\]*](grid/grid.md)
    - [branch](grid/branch/branch.md): interconnects multiple branches in a triangular mesh network
    - [node](grid/node/node.md): interconnects multiple branches in a triangular mesh network
    - [supply](grid/supply/supply.md): provides power supply to the entire demonstrator

## Copyright

2021, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.

## Funding
<a rel="funding" href="https://fismep.de/"><img alt="FISMEP" style="border-width:0" src="docs/fismep_logo.png" width="78" height="63"/></a><br />This work was supported by <a rel="fismep" href="https://fismep.de/">FIWARE for Smart Energy Platform</a> (FISMEP), a German project funded by the *Federal Ministry for Economic Affairs and Energy (BMWi)* and the *ERA-Net Smart Energy Systems* Programme under Grant 0350018A.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Dr. Carlo Guarnieri Calò Carducci](mailto:cguarnieri@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)  

